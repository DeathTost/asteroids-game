package com.proz.asteroids.desktop;

import com.badlogic.gdx.Files;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.proz.asteroids.AsteroidGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title="FUN NOT FOUND! "+AsteroidGame.Version;
		config.useGL30=false;
		config.width=600;
		config.height=500;
		config.resizable=false;
		config.addIcon("logo_ast.png", Files.FileType.Internal);
		new LwjglApplication(new AsteroidGame(), config);
	}
}