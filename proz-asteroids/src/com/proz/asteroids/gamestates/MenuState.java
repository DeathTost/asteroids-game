package com.proz.asteroids.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.proz.asteroids.controller.GameInput;
import com.proz.asteroids.controller.GameStateController;
import com.proz.asteroids.controller.MenuController;
import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;
import com.proz.asteroids.model.World;
import com.proz.asteroids.view.MenuView;

public class MenuState extends State {
	
	private MenuView view;
	private World world;
	private MenuController controller;
	
	public MenuState(GameStateController gsc,int WIDTH,int HEIGHT,InputProcessor InputProcessor,ControllerInput ControllerInput,GameInput GameInput)
	{
		super(gsc,WIDTH,HEIGHT,InputProcessor,ControllerInput,GameInput);
		init();
	}
	
	@Override
	public void init() {
		world = new World(WIDTH,HEIGHT);
		view = new MenuView(world,WIDTH,HEIGHT);
		controller = new MenuController(gsc,view,InputProcessor,ControllerInput,GameInput);
		world.createMenuAsteroids();
	}
	@Override
	public void update(float dt) {
		
		for(int i=0;i<world.getAsteroids().size();i++)
		{
			world.getAsteroids().get(i).update(dt);
		}
		controller.update(dt);	
	}

	@Override
	public void dispose() {
		view.dispose();
		view=null;
		world=null;
		controller=null;
	}

	@Override
	public void draw() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		view.render();
	}
}