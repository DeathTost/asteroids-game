package com.proz.asteroids.gamestates;

import com.proz.asteroids.controller.GameInput;
import com.proz.asteroids.controller.GameStateController;
import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;

public abstract class State {

	protected GameStateController gsc;
	protected InputProcessor InputProcessor;
	protected ControllerInput ControllerInput;
	protected GameInput GameInput;
	protected int WIDTH;
	protected int HEIGHT;
	
	public State(GameStateController gsc,int WIDTH,int HEIGHT,InputProcessor InputProcessor,ControllerInput ControllerInput,GameInput GameInput)
	{
		this.gsc = gsc;
		this.InputProcessor=InputProcessor;
		this.ControllerInput=ControllerInput;
		this.GameInput=GameInput;
		this.WIDTH=WIDTH;
		this.HEIGHT=HEIGHT;
	}
	public abstract void init();
	public abstract void dispose();
	public abstract void update(float dt);
	public abstract void draw();
}