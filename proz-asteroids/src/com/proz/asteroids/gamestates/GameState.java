package com.proz.asteroids.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.proz.asteroids.controller.GameInput;
import com.proz.asteroids.controller.GameStateController;
import com.proz.asteroids.controller.WorldController;
import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;
import com.proz.asteroids.model.World;
import com.proz.asteroids.view.GameView;

public class GameState extends State {

	private GameView view;
	private World world;
	private WorldController controller;
	
	public GameState(GameStateController gsc,int WIDTH,int HEIGHT,InputProcessor InputProcessor,ControllerInput ControllerInput,GameInput GameInput) {
		super(gsc,WIDTH,HEIGHT,InputProcessor,ControllerInput,GameInput);
		init();
	}

	@Override
	public void init() {
		world = new World(WIDTH,HEIGHT);
		world.createWorld();
		view = new GameView(world,WIDTH,HEIGHT);
		controller = new WorldController(gsc,world,InputProcessor,ControllerInput,GameInput);
	}

	@Override
	public void dispose() {
		view.dispose();
		gsc.getGameOverProgressData().setFinalData(world.getPlayer().getScore(), world.getLevel());
		view=null;
		controller=null;
	}

	@Override
	public void update(float dt) {
		controller.update(dt);
	}

	@Override
	public void draw() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		view.render();
	}
}