package com.proz.asteroids.gamestates;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.proz.asteroids.controller.GameInput;
import com.proz.asteroids.controller.GameOverController;
import com.proz.asteroids.controller.GameStateController;
import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;
import com.proz.asteroids.view.GameOverView;

public class GameOverState extends State {

	private GameOverView view;
	private GameOverController controller;
	
	public GameOverState(GameStateController gsc,int WIDTH,int HEIGHT,InputProcessor InputProcessor,ControllerInput ControllerInput,GameInput GameInput) {
		super(gsc,WIDTH,HEIGHT,InputProcessor,ControllerInput,GameInput);
		init();
	}

	@Override
	public void init() {
		view = new GameOverView(WIDTH,HEIGHT,gsc.getGameOverProgressData().getFinalScore(),gsc.getGameOverProgressData().getFinalLevel());
		controller = new GameOverController(gsc,InputProcessor,ControllerInput,GameInput);
	}

	@Override
	public void dispose() {
		view.dispose();
		view=null;
		controller=null;
	}

	@Override
	public void update(float dt) {
		controller.update(dt);
	}

	@Override
	public void draw() {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		view.render();
	}
}