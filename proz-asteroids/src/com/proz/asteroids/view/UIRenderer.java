package com.proz.asteroids.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.proz.asteroids.model.World;

public class UIRenderer {

	private BitmapFont font;
	private SpriteBatch sb;
	private World world;
	private WorldRenderer WorldRenderer;
	
	public UIRenderer(World world,SpriteBatch sb,WorldRenderer WorldRenderer)
	{
		this.world=world;
		this.sb=sb;
		this.WorldRenderer=WorldRenderer;
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("kremlin.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 20;
		font = gen.generateFont(parameter);
		gen.dispose();
	}
	public void render()
	{
		drawLevel();
		drawScore();
		drawLifes();
	}
	private void drawScore()
	{
		sb.setColor(1, 1, 1, 1);
		sb.begin();
		font.draw(sb, Long.toString(world.getPlayer().getScore()),10,470);
		sb.end();
	}
	private void drawLifes()
	{
		for(int i=0; i< world.getPlayer().getLifes();i++)
		{
			world.getHudPlayer().setPosition(20 + i* 24, 430);
			WorldRenderer.getPlayerRenderer().drawPlayer(world.getHudPlayer());
		}
	}
	private void drawLevel()
	{
		sb.setColor(1, 1, 1, 1);
		sb.begin();
		font.draw(sb, "LEVEL "+world.getLevel(), 10, 490);
		sb.end();
	}
	public void dispose()
	{
		font.dispose();
	}
}
