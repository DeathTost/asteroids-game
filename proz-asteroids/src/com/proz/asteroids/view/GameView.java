package com.proz.asteroids.view;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.World;

public class GameView {

	private SpriteBatch sb;
	private ShapeRenderer sr;
	
	private UIRenderer UIRenderer;
	private WorldRenderer WorldRenderer;
	
	public GameView(World world, int WIDTH, int HEIGHT)
	{
		sr = new ShapeRenderer();
		sb = new SpriteBatch();
		WorldRenderer= new WorldRenderer(world,sb,sr);
		UIRenderer= new UIRenderer(world,sb,WorldRenderer);	
	}
	
	public void render()
	{
		sr.setProjectionMatrix(AsteroidGame.cam.combined);
		WorldRenderer.render();
		UIRenderer.render();
	}
	
	public void dispose()
	{
		sr.dispose();
		sb.dispose();
		WorldRenderer.dispose();
		UIRenderer.dispose();
	}
}