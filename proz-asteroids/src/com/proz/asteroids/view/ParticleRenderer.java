package com.proz.asteroids.view;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.proz.asteroids.model.World;

public class ParticleRenderer {
	private World world;
	private ShapeRenderer sr;

	public ParticleRenderer(World world,ShapeRenderer sr)
	{
		this.world = world;
		this.sr=sr;
	}
	
	public void render()
	{
		drawParticles();
	}
	
	public void drawParticles()
	{
		for(int i=0;i<world.getParticles().size();i++)
		{
			drawParticle(i);
		}
	}
	public void drawParticle(int i)
	{
		sr.setColor(1, 1, 1, 1);
		sr.begin(ShapeType.Filled);
		sr.circle(world.getParticles().get(i).getPosition().x, world.getParticles().get(i).getPosition().y, world.getParticles().get(i).getRadius());
		sr.end();
	}
	public void dispose()
	{
		//sr.dispose();
	}
}