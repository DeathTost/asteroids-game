package com.proz.asteroids.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.World;

public class MenuView {
	private BitmapFont font;
	private BitmapFont font1;
	private SpriteBatch sb;
	private ShapeRenderer sr;
	
	private int WIDTH;
	private int HEIGHT;
	private String Title="ASTEROIDS";
	private String[] MenuButtons={"PLAY","EXIT"};
	private int currentButton;
	private AsteroidRenderer AsteroidRenderer;
	
	public MenuView(World world,int WIDTH,int HEIGHT)
	{
		sr = new ShapeRenderer();
		sb = new SpriteBatch();
		this.WIDTH=WIDTH;
		this.HEIGHT=HEIGHT;
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("kremlin.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 60;
		font = gen.generateFont(parameter);
		parameter.size = 40;
		font1 = gen.generateFont(parameter);
		gen.dispose();
		this.AsteroidRenderer=new AsteroidRenderer(world,sr);
	}
	
	public void render()
	{
		sr.setProjectionMatrix(AsteroidGame.cam.combined);
		sb.setProjectionMatrix(AsteroidGame.cam.combined);
		
		drawTitle();
		drawMenuButtons();
		AsteroidRenderer.render();
	}
	private void drawTitle()
	{
		sb.setColor(1, 1, 1, 1);
		sb.begin();
		font.draw(sb,Title,WIDTH/2 - 170,HEIGHT/2+100);
		sb.end();
	}
	
	private void drawMenuButtons()
	{
		sb.setColor(1, 1, 1, 1);
		sb.begin();
		for(int i=0;i<MenuButtons.length;i++)
		{
			drawMenuButton(i,sb);
		}
		sb.end();
	}
	private void drawMenuButton(int i,SpriteBatch sb)
	{
		if(currentButton == i)
		{
			font1.setColor(Color.RED);
		}
		else
		{
			font1.setColor(Color.WHITE);
		}
		font1.draw(sb, MenuButtons[i], WIDTH/2 -60 + 7 * i, 260 - 55 * i);
	}
	public void setCurrentButton(int i)
	{
		currentButton+=i;
	}
	public int getCurrentButton()
	{
		return currentButton;
	}
	public String[] getMenuButtons()
	{
		return MenuButtons;
	}
	
	public void dispose()
	{
		font.dispose();
		font1.dispose();
		sr.dispose();
		sb.dispose();
	}
}
