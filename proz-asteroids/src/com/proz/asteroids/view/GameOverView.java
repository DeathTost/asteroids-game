package com.proz.asteroids.view;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator.FreeTypeFontParameter;
import com.proz.asteroids.AsteroidGame;

public class GameOverView {
	private BitmapFont font;
	private BitmapFont font1;
	private SpriteBatch sb;
	
	private int WIDTH;
	private int HEIGHT;
	private long score;
	private int level;
	private String[] desc;
	
	public GameOverView(int WIDTH,int HEIGHT,long score,int level)
	{
		sb = new SpriteBatch();
		this.WIDTH=WIDTH;
		this.HEIGHT=HEIGHT;
		this.score=score;
		this.level=level;
		FreeTypeFontGenerator gen = new FreeTypeFontGenerator(Gdx.files.internal("kremlin.ttf"));
		FreeTypeFontParameter parameter = new FreeTypeFontParameter();
		parameter.size = 60;
		font = gen.generateFont(parameter);
		parameter.size = 15;
		font1 = gen.generateFont(parameter);
		gen.dispose();
		desc=new String[2];
		setDescribtion(desc,0,"Game Over");
		setDescribtion(desc,1,"Press R or Start button to restart");
	}
	
	private void setDescribtion(String[] desc,int n,String str)
	{
		desc[n]=str;
	}
	public void render()
	{
		sb.setProjectionMatrix(AsteroidGame.cam.combined);
		drawTitle();
		drawFinalScore();
	}
	private void drawTitle()
	{
		sb.setColor(1, 1, 1, 1);
		sb.begin();
		font.draw(sb, desc[0],WIDTH/2 - 180,HEIGHT/2+50);
		font1.draw(sb, desc[1], WIDTH/2 -180, HEIGHT/2 );
		sb.end();
	}
	private void drawFinalScore()
	{
		sb.setColor(1, 1, 1, 1);
		sb.begin();
		font1.draw(sb, "FINAL SCORE: "+score, WIDTH/2 - 80, HEIGHT/2 - 80);
		font1.draw(sb,"FINAL LEVEL: "+level, WIDTH/2 - 80, HEIGHT/2 - 100);
		sb.end();
	}
	
	public void dispose()
	{
		font.dispose();
		font1.dispose();
	}
}