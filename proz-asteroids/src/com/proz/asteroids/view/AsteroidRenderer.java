package com.proz.asteroids.view;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.proz.asteroids.model.Asteroid;
import com.proz.asteroids.model.World;

public class AsteroidRenderer {
	private World world;
	private ShapeRenderer sr;

	public AsteroidRenderer(World world,ShapeRenderer sr)
	{
		this.world = world;
		this.sr=sr;
	}
	public void render()
	{
		drawAsteroids();
	}
	
	public void drawAsteroids()
	{
		for (Asteroid a : world.getAsteroids())
		{
			drawAsteroid(a);
		}
	}
	
	public void drawAsteroid(Asteroid a)
	{
		sr.setColor(1,1,1,1);
		sr.begin(ShapeType.Line);
		for(int i=0, j=a.getShapex().length - 1; i< a.getShapex().length; j=i++)
		{
			sr.line(a.getShapex(i), a.getShapey(i),a.getShapex(j),a.getShapey(j));
		}
		sr.end();
	}
	public void dispose()
	{
		//sr.dispose();
	}
}