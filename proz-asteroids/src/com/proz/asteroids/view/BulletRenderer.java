package com.proz.asteroids.view;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.proz.asteroids.model.World;

public class BulletRenderer {
	private World world;
	private ShapeRenderer sr;

	public BulletRenderer(World world,ShapeRenderer sr)
	{
		this.world = world;
		this.sr=sr;
	}
	
	public void render()
	{
		drawBullets();
	}
	
	public void drawBullets()
	{
		for(int i=0;i<world.getBullet().size();i++)
		{
			drawBullet(i);
		}
	}
	public void drawBullet(int i)
	{
		sr.setColor(1,1,1,1);
		sr.begin(ShapeType.Filled);
		sr.circle(world.getBullet().get(i).getPosition().x, world.getBullet().get(i).getPosition().y, 2);
		sr.end();
	}
	public void dispose()
	{
		//sr.dispose();
	}
}