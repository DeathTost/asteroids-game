package com.proz.asteroids.view;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.proz.asteroids.model.Player;
import com.proz.asteroids.model.World;

public class PlayerRenderer {
	private World world;
	private ShapeRenderer sr;

	public PlayerRenderer(World world,ShapeRenderer sr)
	{
		this.world = world;
		this.sr=sr;
	}
	
	public void render()
	{
		if(world.getPlayer().isHit())
		{
			drawHitLines();
		}
		else
		{
			drawPlayer(world.getPlayer());
			if(world.getPlayer().getForwardMovement())
			{
				drawFlame();
			}
		}
	}
	public void drawPlayer(Player player)
	{
		sr.setColor(1,1,1,1);
		sr.begin(ShapeType.Line);
		for(int i=0, j=player.getShapex().length - 1; i< player.getShapex().length; j=i++)
		{
			sr.line(player.getShapex(i), player.getShapey(i),player.getShapex(j),player.getShapey(j));
		}
		sr.end();
	}
	public void drawFlame()
	{
		sr.setColor(1, 1, 1, 1);
		sr.begin(ShapeType.Line);
		if(world.getPlayer().getForwardMovement())
		{
			for(int i=0, j=world.getPlayer().getFlamex().length-1;i<world.getPlayer().getFlamex().length;j=i++)
			{
				sr.line(world.getPlayer().getFlamex(i), world.getPlayer().getFlamey(i),
						world.getPlayer().getFlamex(j),world.getPlayer().getFlamey(j));
			}
		}
		sr.end();
	}
	
	public void drawHitLines()
	{
		sr.setColor(1, 1, 1, 1);
		sr.begin(ShapeType.Line);
		for(int i=0;i<world.getPlayer().getHitLines().length;i++)
		{
			sr.line(world.getPlayer().getHitLines(i).x1,world.getPlayer().getHitLines(i).y1,
					world.getPlayer().getHitLines(i).x2,world.getPlayer().getHitLines(i).y2);		
		}
		sr.end();
	}
	public void dispose()
	{
		//sr.dispose();
	}
}