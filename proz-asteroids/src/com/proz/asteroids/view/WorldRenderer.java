package com.proz.asteroids.view;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.proz.asteroids.model.World;

public class WorldRenderer {

	private PlayerRenderer PlayerRenderer;
	private AsteroidRenderer AsteroidRenderer;
	private ParticleRenderer ParticleRenderer;
	private BulletRenderer BulletRenderer;
	
	public WorldRenderer(World world, SpriteBatch sb, ShapeRenderer sr)
	{		
		PlayerRenderer=new PlayerRenderer(world,sr);
		AsteroidRenderer=new AsteroidRenderer(world,sr);
		ParticleRenderer=new ParticleRenderer(world,sr);
		BulletRenderer=new BulletRenderer(world,sr);
	}
	
	public void render()
	{
		PlayerRenderer.render();
		AsteroidRenderer.render();
		BulletRenderer.render();
		ParticleRenderer.render();
	}
	
	public PlayerRenderer getPlayerRenderer()
	{
		return PlayerRenderer;
	}
	
	public AsteroidRenderer getAsteroidRenderer()
	{
		return AsteroidRenderer;
	}
	
	public void dispose()
	{
		PlayerRenderer.dispose();
		AsteroidRenderer.dispose();
		BulletRenderer.dispose();
		ParticleRenderer.dispose();
	}
}