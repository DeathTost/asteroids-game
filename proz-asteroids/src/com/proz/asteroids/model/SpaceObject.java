package com.proz.asteroids.model;

import com.badlogic.gdx.math.Vector2;
import com.proz.asteroids.AsteroidGame;

public abstract class SpaceObject {

	protected float dx;
	protected float dy;
	protected Vector2 position=new Vector2();
	protected float radians;
	protected float speed;
	protected float rotationSpeed;
	protected float[] shapex;
	protected float[] shapey;
	
	public void setPosition(Vector2 pos)
	{
		this.position = pos;
	}
	public Vector2 getPosition()
	{
		return position;
	}
	public float getSpeed()
	{
		return speed;
	}
	public float getRadians()
	{
		return radians;
	}
	public float getRotationSpeed()
	{
		return rotationSpeed;
	}
	public float[] getShapex() {
		return shapex;
	}
	public float getShapex(int x)
	{
		return this.shapex[x];
	}
	public float getShapey(int y)
	{
		return this.shapey[y];
	}
	public float[] getShapey() {
		return shapey;
	}

	public void setPosition(float x,float y)
	{
		this.position.x=x;
		this.position.y=y;
	}
	
	//http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html
	//odd - inside polygon, even - outside polygon
	public boolean contains(float x, float y)
	{
		if(shapex == null) return false;
		boolean b=false;
		for(int i=0, j=shapex.length -1;i< shapex.length;j=i++)
		{
			if((shapey[i]>y) != (shapey[j]>y) && (x<(shapex[j]-shapex[i])*(y-shapey[i])/(shapey[j]-shapey[i])+shapex[i]))
			{
				b=!b;
			}
		}
		return b;
	}
	
	public boolean intersects(SpaceObject other)
	{
		if(other == this) return false;
		if(this.getShapex()==null || this.getShapey()==null) return false;
		float[] sx = other.getShapex();
		float[] sy = other.getShapey();
		if(sx == null || sy==null) return false;
		
		for(int i=0;i<sx.length;i++)
		{
			if(contains(sx[i],sy[i]))
			{
				return true;
			}
		}
		
		for(int i=0; i<this.getShapex().length;i++)
		{
			if(other.contains(this.shapex[i], this.shapey[i]))
			{
				return true;
			}
		}
		return false;
	}
	public void wrap(SpaceObject obj)
	{
		if(obj != this) return;
		
		if(obj.position.x <0) obj.position.x=AsteroidGame.WIDTH;
		if(obj.position.x > AsteroidGame.WIDTH) obj.position.x=0;
		if(obj.position.y < 0) obj.position.y=AsteroidGame.HEIGHT;
		if(obj.position.y > AsteroidGame.HEIGHT) obj.position.y=0;
	}
}