package com.proz.asteroids.model;

public class GameOverProgressData {

	private long final_score=0;
	private int final_level=1;
	
	public void setFinalData(long score,int level)
	{
		if(score < 0 || level < 1) return;
		this.final_score=score;
		this.final_level=level;
	}
	public long getFinalScore()
	{
		return final_score;
	}
	public int getFinalLevel()
	{
		return final_level;
	}
}