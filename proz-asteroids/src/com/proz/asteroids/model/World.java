package com.proz.asteroids.model;

import java.util.ArrayList;
import java.util.List;
import com.badlogic.gdx.math.MathUtils;
import com.proz.asteroids.model.numbergenerator.*;

public class World {

	private int WIDTH;
	private int HEIGHT;
	private Player player;
	private Player hudPlayer;
	private ArrayList<Particle> particles;
	private List<Asteroid> ast;
	private ArrayList<Bullet> bullets;
	private int level;
	private NumberGenerator NumGen;
	 
	public World(int WIDTH,int HEIGHT)
	{
		this.WIDTH=WIDTH;
		this.HEIGHT=HEIGHT;
		this.NumGen = new RandomNumberGenerator();
	}
	
	public void createWorld()
	{
		ast=new ArrayList<Asteroid>();
		bullets = new ArrayList<Bullet>();
		player=new Player(bullets);
		particles = new ArrayList<Particle>();
		hudPlayer = new Player(null);
		level =1;
		spawnAsteroids();    
	}
	
	public void createMenuAsteroids()
	{
		ast=new ArrayList<Asteroid>();
		spawnAsteroids(10);
	}
	
	//Spawn asteroids for Game with existing player
	private void spawnAsteroids()
	{
		ast.clear();
		int numToSpawn = 4 + level -1;
		for(int i=0;i < numToSpawn; i++)
		{
			float x = (float)MathUtils.random(this.WIDTH);
			float y = (float)MathUtils.random(this.HEIGHT);
			
			float dx= x - getPlayer().getPosition().x;
			float dy=y - getPlayer().getPosition().y;
			float dist = (float) Math.sqrt(dx*dx+dy*dy);
			
			while(dist < 100)
			{
				x = (float)MathUtils.random(this.WIDTH);
				y = (float)MathUtils.random(this.HEIGHT);
				dx= x - getPlayer().getPosition().x;
				dy= y - getPlayer().getPosition().y;
				dist = (float) Math.sqrt(dx*dx+dy*dy);
			}
			ast.add(new Asteroid(x,y,2,NumGen));
		}
	}
	
	//Spawn asteroids for MainMenu without existing player
	void spawnAsteroids(int numToSpawn)
	{
		ast.clear();
		if(numToSpawn < 0) return;
		for(int i=0;i < numToSpawn; i++)
		{
			float x = (float)MathUtils.random(this.WIDTH);
			float y = (float)MathUtils.random(this.HEIGHT);
			int size= MathUtils.random(0, 2);
			ast.add(new Asteroid(x,y,size,NumGen));
		}
	}
	
	void splitAsteroids(Asteroid a)
	{
		createParticles(a.getPosition().x,a.getPosition().y);
		if(a.getType() == Asteroid.BIG)
		{
			ast.add(new Asteroid(a.getPosition().x,a.getPosition().y,Asteroid.MEDIUM,NumGen));
			ast.add(new Asteroid(a.getPosition().x,a.getPosition().y,Asteroid.MEDIUM,NumGen));
		}
		if(a.getType() == Asteroid.MEDIUM)
		{
			ast.add(new Asteroid(a.getPosition().x,a.getPosition().y,Asteroid.SMALL,NumGen));
			ast.add(new Asteroid(a.getPosition().x,a.getPosition().y,Asteroid.SMALL,NumGen));
		}
	}

	private void createParticles(float x,float y)
	{
		createParticles(x,y,6);
	}
	
	private void createParticles(float x,float y, int amount)
	{
		for(int i=0; i<amount;i++)
		{
			particles.add(new Particle(x,y,NumGen));		
		}
	}
	
	public ArrayList<Bullet> getBullet()
	{
		return bullets;
	}
	
	public Player getPlayer()
	{
		return player;
	}
	public Player getHudPlayer()
	{
		return hudPlayer;
	}
	public List<Particle> getParticles()
	{
		return particles;
	}
	
	public List<Asteroid> getAsteroids()
	{
		return ast;
	}
	public int getLevel()
	{
		return level;
	}
	
	public void update(float dt)
	{
		LevelUpdate();
		
		player.update(dt);
		if(player.isDead())
		{
			player.reset();
			player.loseLife();
			return;
		}
		
		BulletsUpdate(dt);
		AsteroidUpdate(dt);
		ParticlesUpdate(dt);
		checkCollisions();
	}
	private void LevelUpdate()
	{
		if(ast.size() == 0)
		{
			level++;
			spawnAsteroids();
		}
	}
	private void BulletsUpdate(float dt)
	{
		for(int i=0;i<bullets.size();i++)
		{
			bullets.get(i).update(dt);
			if(bullets.get(i).shouldRemove())
			{
				bullets.remove(i);
				i--;
			}
		}
	}
	private void AsteroidUpdate(float dt)
	{
		for(Asteroid a: ast)
		{
			a.update(dt);	
		}
	}
	private void ParticlesUpdate(float dt)
	{
		for(int i=0; i <particles.size();i++)
		{
			particles.get(i).update(dt);
			if(particles.get(i).shouldRemove())
			{
				particles.remove(i);
				i--;
			}
		}
	}
	private void checkCollisions()
	{
		PlayerAsteroidCollision();
		BulletAsteroidCollision();	
	}
	
	void PlayerAsteroidCollision()
	{
		if(!player.isHit())
		{
			//player-asteroid collision
			for(int i=0;i<ast.size();i++)
			{
				Asteroid a= ast.get(i);
				//player intersects asteroid
				if(a.intersects(player))
				{
					player.hitIndicated();
					ast.remove(i);
					i--;
					splitAsteroids(a);
					break;	
				}
			}
		}
	}
	void BulletAsteroidCollision()
	{
		//bullet-asteroid collision
		for(int i=0;i < bullets.size(); i++)
		{
			Bullet b = bullets.get(i);
			for(int j=0;j<ast.size();j++)
			{
				Asteroid a= ast.get(j);
				//a contains b
				if(a.contains(b.getPosition().x, b.getPosition().y))
				{
					bullets.remove(i);
					i--;
					
					ast.remove(j);
					j--;
					
					splitAsteroids(a);
					player.increaseScore(a.getScore());
					break;
				}
			}
		}	
	}
}