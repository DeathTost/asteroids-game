package com.proz.asteroids.model;

import com.badlogic.gdx.math.MathUtils;

public class Bullet extends SpaceObject {

	private float lifeTime;
	private float lifeTimer;
	private boolean removeIndicator;
	
	public Bullet(float x,float y,float radians)
	{
		this.position.x=x;
		this.position.y=y;
		this.radians=radians;
		
		this.speed=350;
		dx=MathUtils.cos(radians)*speed;
		dy=MathUtils.sin(radians)*speed;
		
		lifeTimer=0;
		lifeTime=1;
		removeIndicator=false;
	}
	
	public boolean shouldRemove()
	{
		return removeIndicator;
	}
	
	public void update(float dt)
	{
		this.position.x+=dx*dt;
		this.position.y+=dy*dt;
		
		wrap(this);
		
		lifeTimer +=dt;
		
		if(lifeTimer > lifeTime)
		{
			removeIndicator=true;
		}
	}
}