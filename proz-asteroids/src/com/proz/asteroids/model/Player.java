package com.proz.asteroids.model;

import java.awt.geom.Line2D;
import java.awt.geom.Line2D.Float;
import java.awt.geom.Point2D;

import java.util.ArrayList;

import com.badlogic.gdx.math.MathUtils;
import com.proz.asteroids.AsteroidGame;

public class Player extends SpaceObject {
	
	private ArrayList<Bullet> bullets;
	private final int MAX_BULLETS=4;
	
	private float maxSpeed;
	private float acceleration;
	private float deacceleration;
	private float accelerationTimer;
	
	private int ExtraLifes;
	private long score;
	private long requiredScore;
	
	private boolean isHit;
	private boolean isDead;
	private boolean isLeftRotation=false,isRightRotation=false,isForwardMovement=false;
	
	private float[] flamex;
	private float[] flamey;
	private Line2D.Float[] ShipwreckLines;
	private Point2D.Float[] ShipwreckLinesVector;
	private float hitTimer;
	private float hitTime;
	
	public Player(ArrayList<Bullet> bullets)
	{
		this.bullets=bullets;
		this.isDead=false;
		this.isHit=false;
		this.position.x = AsteroidGame.WIDTH/2;
		this.position.y = AsteroidGame.HEIGHT/2;
		
		this.maxSpeed = 300;
		this.acceleration = 200;
		this.deacceleration = 50;
		this.rotationSpeed =3;
		this.radians = 3.1415f/2;
		this.accelerationTimer=0;
		
		shapex = new float[4];
		shapey = new float[4];
		flamex = new float[3];
		flamey = new float[3];
		
		score=0;
		setLife(3);
		requiredScore=10000;
		hitTimer=0;
		hitTime=2;	
		setShape();
	}
	
	public void setLeftRotationEnabled(boolean b)
	{
		this.isLeftRotation=b;
	}
	public void setRightRotationEnabled(boolean b)
	{
		this.isRightRotation=b;
	}
	public void setForwardMovementEnabled(boolean b)
	{
		this.isForwardMovement=b;
	}
	public boolean getLeftRotation()
	{
		return isLeftRotation;
	}
	public boolean getRightRotation()
	{
		return isRightRotation;
	}
	public boolean getForwardMovement()
	{
		return isForwardMovement;
	}
	public float[] getFlamex()
	{
		return flamex;
	}
	public float[] getFlamey()
	{
		return flamey;
	}
	public float getFlamex(int x)
	{
		return this.flamex[x];
	}
	public float getFlamey(int y)
	{
		return this.flamey[y];
	}
	public void shoot()
	{	
		if(bullets == null) return;
		if(!isHit())
		{
			if(bullets.size() == MAX_BULLETS)
			{
				return;
			}
			bullets.add(new Bullet(shapex[0],shapey[0],this.radians));	
		}
	}
	public ArrayList<Bullet> getPlayerBullets()
	{
		return bullets;
	}
	public Line2D.Float[] getHitLines()
	{
		return ShipwreckLines;
	}
	public Float getHitLines(int k)
	{
		return ShipwreckLines[k];
	}
	
	public void setLife(int x)
	{
		if(x<0) x=0;
		this.ExtraLifes=x;
	}
	public void addLife(int x)
	{
		if(x < -this.ExtraLifes)
		{
		this.ExtraLifes=-1; //lost every life, should be dead permanently
		return;
		}
		this.ExtraLifes+=x;
	}
	
	public int getLifes()
	{
		return this.ExtraLifes;
	}
	public void loseLife()
	{
		if(ExtraLifes < 0) return;
		ExtraLifes--;
	}
	public void checkExtraLives()
	{
		if(score >= requiredScore)
		{
			addLife(1);
			requiredScore += 10000;
		}
	}
	public long getScore()
	{
		return score;
	}
	public void increaseScore(long l)
	{
		if(l < 0) return;
		score+=l;
	}
	public boolean isDead()
	{
		return isDead;
	}
	public boolean isHit()
	{
		return isHit;
	}
	
	public void hitIndicated()
	{
		if(isHit) return;
		
		isHit=true;
		dx=dy=0;
		isLeftRotation=isRightRotation=isForwardMovement=false;
		
		ShipwreckLines = new Line2D.Float[4];
		for(int i=0, j=ShipwreckLines.length-1;i<ShipwreckLines.length;j=i++)
		{
			ShipwreckLines[i]= new Line2D.Float(shapex[i], shapey[i], shapex[j], shapey[j]);
		}
		
		ShipwreckLinesVector = new Point2D.Float[4];
		ShipwreckLinesVector[0] = new Point2D.Float(MathUtils.cos(radians+1.5f), MathUtils.sin(radians + 1.5f));
		ShipwreckLinesVector[1] = new Point2D.Float(MathUtils.cos(radians-1.5f), MathUtils.sin(radians - 1.5f));
		ShipwreckLinesVector[2] = new Point2D.Float(MathUtils.cos(radians-2.8f), MathUtils.sin(radians - 2.8f));
		ShipwreckLinesVector[3] = new Point2D.Float(MathUtils.cos(radians+2.8f), MathUtils.sin(radians + 2.8f));
	}

	public void setPosition(float x,float y)
	{
		super.setPosition(x, y);
		setShape();
	}
	public void reset()
	{
		super.setPosition(AsteroidGame.WIDTH/2,AsteroidGame.HEIGHT/2);
		setShape();
		isHit=isDead=false;
	}
	private void setShape()
	{
		shapex[0]= this.position.x + MathUtils.cos(radians) * 16; //gora
		shapey[0]= this.position.y + MathUtils.sin(radians) * 16;
		shapex[1]= this.position.x + MathUtils.cos(radians - 4 * 3.1415f/5) * 16; //lewa
		shapey[1]= this.position.y + MathUtils.sin(radians - 4 * 3.1415f/5) * 16;
		shapex[2]= this.position.x + MathUtils.cos(radians + 3.1415f) * 4;		//srodek
		shapey[2]= this.position.y + MathUtils.sin(radians + 3.1415f) * 4;
		shapex[3]= this.position.x + MathUtils.cos(radians + 4 * 3.1415f/5) * 16; //prawa
		shapey[3]= this.position.y + MathUtils.sin(radians + 4 * 3.1415f/5) * 16;		
	}
	
	private void setFlame()
	{
		flamex[0]= this.position.x + MathUtils.cos(radians - 5*3.1415f/6) * 10;//lewe
		flamey[0]= this.position.y + MathUtils.sin(radians - 5*3.1415f/6) * 10;

		flamex[1]= this.position.x + MathUtils.cos(radians - 3.1415f) * (14 + accelerationTimer * 44);//dol
		flamey[1]= this.position.y + MathUtils.sin(radians - 3.1415f) * (14 + accelerationTimer * 44);
		
		flamex[2]= this.position.x + MathUtils.cos(radians + 5*3.1415f/6) * 10;//prawe
		flamey[2]= this.position.y + MathUtils.sin(radians + 5*3.1415f/6) * 10;
	}

	public void update(float dt)
	{
		if(isHit)
		{
			ShipwreckMovement(dt);
			return;
		}
		checkExtraLives();
		rotate(dt);
		acceleration(dt);
		deacceleration(dt);
		setPosition(dt);
		setShape();
		if(isForwardMovement)
		{
			setFlame();
		}
		wrap(this);
	}

	private void ShipwreckMovement(float dt)
	{
		hitTimer +=dt;
		if(hitTimer > hitTime)
		{
			isDead=true;
			hitTimer=0;
		}
		for(int i=0; i< ShipwreckLines.length;i++)
		{
			ShipwreckLines[i].setLine(ShipwreckLines[i].x1+ShipwreckLinesVector[i].x * 10 * dt,ShipwreckLines[i].y1+ShipwreckLinesVector[i].y * 10 * dt ,
					ShipwreckLines[i].x2+ShipwreckLinesVector[i].x * 10 * dt, ShipwreckLines[i].y2+ShipwreckLinesVector[i].y * 10 * dt);
		}
	}
	void setPosition(float dt)
	{
		this.position.x +=dx * dt;
		this.position.y +=dy * dt;
	}
	void acceleration(float dt)
	{
		if(isForwardMovement)
		{
			dx += MathUtils.cos(radians)*acceleration * dt;
			dy += MathUtils.sin(radians)*acceleration * dt;

			accelerationTimer += dt;
			if(accelerationTimer > 0.1f)
			{
				accelerationTimer= 0;
			}
		}
		else
		{
			accelerationTimer =0;
		}
	}
	void deacceleration(float dt)
	{
		float speed = (float) Math.sqrt(dx*dx + dy*dy);
		if(speed > 0)
		{
			dx -= (dx/speed)* deacceleration * dt;
			dy -= (dy/speed)* deacceleration * dt;
		}
		if(speed > maxSpeed)
		{
			dx = (dx/speed)* maxSpeed ;
			dy = (dy/speed)* maxSpeed ;
		}
	}
	void rotate(float dt)
	{
		if(isLeftRotation && isRightRotation) return;
		if(isLeftRotation)
		{
			this.radians += rotationSpeed * dt;
		}
		if(isRightRotation)
		{
			this.radians -= rotationSpeed * dt;
		}	
	}
}