package com.proz.asteroids.model;

import com.badlogic.gdx.math.MathUtils;
import com.proz.asteroids.model.numbergenerator.NumberGenerator;

public class Asteroid extends SpaceObject {
	public static final int SMALL=0;
	public static final int MEDIUM=1;
	public static final int BIG=2;

	private int typeSize;
	private int score;
	private int numVertices;
	private float[] distance;
	private int radius;

	public Asteroid(float x,float y,int type,NumberGenerator NumGen)
	{
		this.typeSize = type;
		if(type < SMALL) 
		{
			this.typeSize=SMALL;
		}
		if(type > BIG)
		{
			this.typeSize=BIG;
		}
		
		setPosition(x,y);

		if(this.typeSize == SMALL)
		{
			setAsteroidParameters(8,10,70,100,100,NumGen);//numVertices//radius//startSpeed//endSpeed//Score//NumGen
		}
		else if(this.typeSize == MEDIUM)
		{
			setAsteroidParameters(10,22,50,60,50,NumGen);
		}
		else if(this.typeSize == BIG)
		{
			setAsteroidParameters(12,40,20,30,20,NumGen);
		}
		this.rotationSpeed = NumGen.getIntValueFromRange(-1, 1);
		radians = NumGen.getFloatValue(2*3.1415f);
		dx = MathUtils.cos(radians)*speed;
		dy = MathUtils.sin(radians)*speed;
		
		shapex = new float[numVertices];
		shapey = new float[numVertices];
		distance = new float[numVertices];
		
		for(int i=0;i<numVertices;i++)
		{
			distance[i]= NumGen.getIntValueFromRange(radius/2, radius);
		}
		setShape();
	}
	private void setAsteroidParameters(int numVertices,int radius,int startSpeed,int endSpeed,int score,NumberGenerator NumGen)
	{
		this.numVertices=numVertices;
		this.radius=radius;
		this.speed = NumGen.getIntValueFromRange(startSpeed, endSpeed);
		this.score=score;
	}
	private void setShape()
	{
		float angle=0;
		for(int i=0;i<this.numVertices;i++)
		{
			this.shapex[i]=this.position.x+MathUtils.cos(angle+radians)*distance[i];
			this.shapey[i]=this.position.y+MathUtils.sin(angle+radians)*distance[i];
			angle+= 2*3.1415f/this.numVertices;
		}
	}
	
	public void update(float dt)
	{
		this.position.x+=dx*dt;
		this.position.y+=dy*dt;
		
		radians += rotationSpeed *dt;
		setShape();
		wrap(this);
	}
	
	public int getType()
	{
		return typeSize;
	}
	public int getScore()
	{
		return score;
	}
	public int getRadius()
	{
		return radius;
	}
}