package com.proz.asteroids.model;

import com.badlogic.gdx.math.MathUtils;
import com.proz.asteroids.model.numbergenerator.NumberGenerator;

public class Particle extends SpaceObject {

	private float lifeTimer;
	private float lifeTime;
	private boolean removeIndicator;
	private float radius;
	
	public Particle(float x,float y,NumberGenerator NumGen)
	{
		this.position.x = x;
		this.position.y = y;
		radius=1;
		speed=50;
		radians = NumGen.getFloatValue(2*3.1415f);
		dx = MathUtils.cos(radians)*speed;
		dy = MathUtils.sin(radians)*speed;
		
		lifeTimer=0;
		lifeTime=1;
	}
	
	public float getRadius()
	{
		return radius;
	}
	public boolean shouldRemove()
	{
		return removeIndicator;
	}
	
	public void update(float dt)
	{
		this.position.x+=dx*dt;
		this.position.y+=dy*dt;
		
		lifeTimer += dt;
		if(lifeTimer > lifeTime)
		{
			removeIndicator = true;
		}
	}
}