package com.proz.asteroids.model.numbergenerator;

import com.badlogic.gdx.math.MathUtils;

public class RandomNumberGenerator extends NumberGenerator{

	public int getIntValueFromRange(int start,int end)
	{
		return MathUtils.random(start, end);
	}
	
	public float getFloatValue(float range)
	{
		return MathUtils.random(range);
	}
	
}
