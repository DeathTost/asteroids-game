package com.proz.asteroids.model.numbergenerator;

public class ConstantNumberGenerator extends NumberGenerator {
	
	public int getIntValueFromRange(int start,int end)
	{
		return start;
	}
	
	public float getFloatValue(float range)
	{
		return range;
	}
}
