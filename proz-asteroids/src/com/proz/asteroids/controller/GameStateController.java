package com.proz.asteroids.controller;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.utils.Array;
import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;
import com.proz.asteroids.gamestates.GameOverState;
import com.proz.asteroids.gamestates.GameState;
import com.proz.asteroids.gamestates.MenuState;
import com.proz.asteroids.gamestates.State;
import com.proz.asteroids.model.GameOverProgressData;

public class GameStateController {

	public State currentState;
	public static final int MENU=0;
	public static final int PLAY=1;
	public static final int GAMEOVER=2;
	
	private Array<Controller> controllers=Controllers.getControllers();
	private InputProcessor InputProcessor;
	private ControllerInput ControllerInput;
	private GameInput GameInput;
	private int WIDTH;
	private int HEIGHT;
	private GameOverProgressData GOPD;
	
	public GameStateController(int WIDTH,int HEIGHT)
	{
		this.GOPD = new GameOverProgressData();
		this.WIDTH=WIDTH;
		this.HEIGHT=HEIGHT;
		setInput();
		setState(MENU,WIDTH,HEIGHT);
	}
	
	public void setState(int state)
	{
		setState(state,WIDTH,HEIGHT);
	}
	public State getState()
	{
		return currentState;
	}
	private void setState(int state,int WIDTH,int HEIGHT)
	{
		if(currentState != null) currentState.dispose();
		if(state == MENU)
		{
			this.currentState= new MenuState(this,WIDTH,HEIGHT,InputProcessor,ControllerInput,GameInput);
		}
		if(state == PLAY)
		{
			this.currentState= new GameState(this,WIDTH,HEIGHT,InputProcessor,ControllerInput,GameInput);
		}
		if(state == GAMEOVER)
		{
			this.currentState= new GameOverState(this,WIDTH,HEIGHT,InputProcessor,ControllerInput,GameInput);
		}
	}
	public void update(float dt)
	{
		this.currentState.update(dt);
	}
	public void render()
	{
		this.currentState.draw();
	}
	public GameOverProgressData getGameOverProgressData()
	{
		return GOPD;
	}
	private void setInput()
	{
		this.GameInput=new GameInput();
		this.InputProcessor=new InputProcessor(GameInput);
		Gdx.input.setInputProcessor(InputProcessor);
		
		if(controllers.size == 0)
		{
			//nothing
		}
		else
		{
			Controller pad=null;
			for(Controller c:controllers)
			{
				if(c.getName().contains("Xbox") && c.getName().contains("360"))
				{
					pad=c;
					this.ControllerInput=new ControllerInput(GameInput);
					pad.addListener(ControllerInput);
				}
			}			
		}
	}
}