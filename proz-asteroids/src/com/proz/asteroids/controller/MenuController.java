package com.proz.asteroids.controller;

import com.badlogic.gdx.Gdx;
import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;

public class MenuController {

	private GameInput GameInput;
	private InputProcessor InputProcessor;
	private ControllerInput ControllerInput;
	private com.proz.asteroids.view.MenuView MenuView;
	private GameStateController gsc;
	
	public MenuController(GameStateController gsc,com.proz.asteroids.view.MenuView MenuView,InputProcessor InputProcessor,ControllerInput ControllerInput,GameInput GameInput)
	{	
		this.gsc=gsc;
		this.MenuView=MenuView;
		this.GameInput=GameInput;
		this.InputProcessor=InputProcessor;
		this.ControllerInput=ControllerInput;
		this.GameInput.clearInput();
	}
	public void update(float dt)
	{
		processInput();
		InputProcessor.update();
		if(ControllerInput != null)
		{
			ControllerInput.update();
		}
	}
	
	private void processInput()
	{
		if(GameInput.isUpIndicated())
		{
			if(MenuView.getCurrentButton() > 0)
			{
				MenuView.setCurrentButton(-1);
			}		
		}
		if(GameInput.isDownIndicated())
		{
			if(MenuView.getCurrentButton() < MenuView.getMenuButtons().length-1)
			{
				MenuView.setCurrentButton(1);
			}
		}
		if(GameInput.isAccepted())
		{
			select();
		}
		if(GameInput.isExitIndicated())
		{
			Gdx.app.exit();
		}
	}	
	private void select()
	{
		if(MenuView.getCurrentButton() == 0)
		{
			gsc.setState(GameStateController.PLAY);
		}
		if(MenuView.getCurrentButton() == 1)
		{
			Gdx.app.exit();
		}
	}
}
