package com.proz.asteroids.controller.input;

public interface GameKeys {

	static final int THRUST=0;
	static final int LEFT_ROT=1;
	static final int RIGHT_ROT=2;
	static final int ATTACK=3;
	static final int EXIT=4;
	static final int UP=5;
	static final int DOWN=6;
	static final int ACCEPT=7;
	static final int RESTART=8;
	static final int NUM_KEYS=9;

	public static void update() {}
	public static void setKey(int k, boolean b) {}
	public static boolean isDown(int k) {	return false;}
	public static boolean isPressed(int k) {	return false;}
}
