package com.proz.asteroids.controller.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.ControllerListener;
import com.badlogic.gdx.controllers.PovDirection;
import com.badlogic.gdx.math.Vector3;
import com.proz.asteroids.controller.GameInput;

public class ControllerInput implements ControllerListener {

	public ControllerInput(GameInput g_in)
	{
		XboxKeys.setGameInput(g_in);
	}
	
	public void update()
	{
		XboxKeys.update();
	}
	
	@Override
	public void connected(Controller controller) {
		Gdx.app.log("Controller", "activated");
	}

	@Override
	public void disconnected(Controller controller) {
		Gdx.app.log("Controller","deactivated");
	}

	@Override
	public boolean buttonDown(Controller controller, int buttonCode) {
		
		if(buttonCode == XboxKeys.BUTTON_A)
		{
			XboxKeys.setKey(GameKeys.ATTACK, true);
			XboxKeys.setKey(GameKeys.ACCEPT, true);
		}
		else if(buttonCode == XboxKeys.BUTTON_RB)
		{
			XboxKeys.setKey(GameKeys.THRUST, true);
		}
		else if(buttonCode == XboxKeys.BUTTON_START)
		{
			XboxKeys.setKey(GameKeys.EXIT, true);
		}
		else if(buttonCode == XboxKeys.BUTTON_BACK)
		{
			XboxKeys.setKey(GameKeys.RESTART, true);
		}
		return true;
	}

	@Override
	public boolean buttonUp(Controller controller, int buttonCode) {
		if(buttonCode == XboxKeys.BUTTON_A)
		{
			XboxKeys.setKey(GameKeys.ATTACK, false);
			XboxKeys.setKey(GameKeys.ACCEPT, false);
		}
		else if(buttonCode == XboxKeys.BUTTON_RB)
		{
			XboxKeys.setKey(GameKeys.THRUST, false);
		}
		else if(buttonCode == XboxKeys.BUTTON_START)
		{
			XboxKeys.setKey(GameKeys.EXIT, false);
		}
		else if(buttonCode == XboxKeys.BUTTON_BACK)
		{
			XboxKeys.setKey(GameKeys.RESTART, false);
		}
		return true;
	}

	@Override
	public boolean axisMoved(Controller controller, int axisCode, float value) {
		if(axisCode == XboxKeys.AXIS_LX)
		{
			if(value > 0.2)
			{
				XboxKeys.setKey(GameKeys.LEFT_ROT, false);
				XboxKeys.setKey(GameKeys.RIGHT_ROT, true);
			}
			else if(value < -0.2)
			{
				XboxKeys.setKey(GameKeys.RIGHT_ROT, false);
				XboxKeys.setKey(GameKeys.LEFT_ROT, true);
			}
			if(value > -0.2 && value < 0.2)
			{
				XboxKeys.setKey(GameKeys.LEFT_ROT, false);
				XboxKeys.setKey(GameKeys.RIGHT_ROT, false);
			}
		}
		if(axisCode == XboxKeys.AXIS_LY)
		{
			if(value > 0.2)
			{
				XboxKeys.setKey(GameKeys.UP, false);
				XboxKeys.setKey(GameKeys.DOWN, true);
			}
			else if(value < -0.2)
			{
				XboxKeys.setKey(GameKeys.DOWN, false);
				XboxKeys.setKey(GameKeys.UP, true);
			}
			if(value > -0.2 && value < 0.2)
			{
				XboxKeys.setKey(GameKeys.UP, false);
				XboxKeys.setKey(GameKeys.DOWN, false);
			}
		}

		if(axisCode == XboxKeys.AXIS_TRIGGER)
		{
			if(value < -0.2)
			{
				XboxKeys.setKey(GameKeys.THRUST, true);
			}
			else
			{
				XboxKeys.setKey(GameKeys.THRUST, false);
			}
		}	
		return true;
	}
	@Override
	public boolean povMoved(Controller controller, int povCode, PovDirection value) {
		return true;
	}
	@Override
	public boolean xSliderMoved(Controller controller, int sliderCode, boolean value) {
		return false;
	}
	@Override
	public boolean ySliderMoved(Controller controller, int sliderCode, boolean value) {
		return false;
	}
	@Override
	public boolean accelerometerMoved(Controller controller, int accelerometerCode, Vector3 value) {
		return false;
	}
}