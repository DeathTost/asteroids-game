package com.proz.asteroids.controller.input;

import com.proz.asteroids.controller.GameInput;

abstract public class DesktopKeys implements GameKeys {

	public static final int UP=0;
	public static final int LEFT=1;
	public static final int RIGHT=2;
	public static final int SPACE=3;
	public static final int ESCAPE=4;
	public static final int DOWN = 5;
	public static final int ENTER=6;
	public static final int R=7;
	
	private static boolean[] keys=new boolean[NUM_KEYS];
	private static boolean[] pkeys=new boolean[NUM_KEYS];
	private static GameInput GameInput;
	
	public static void update()
	{
		changeKeyPressed(GameKeys.ATTACK);
		for(int i=0;i<NUM_KEYS;i++)
		{
			changeKey(i);
			pkeys[i]=keys[i];
		}
	}
	public static void setGameInput(GameInput GI)
	{
		GameInput=GI;
	}
	public static void setKey(int k, boolean b)
	{
		keys[k]=b;
	}
	public static boolean isDown(int k)
	{
		return keys[k];
	}
	public static boolean isPressed(int k)
	{
		return keys[k] && !pkeys[k];
	}
	public static void changeKey(int num)
	{
		if(isPressed(num))
		{
			GameInput.setKey(num,1);
		}
		if(pkeys[num]==true && keys[num]==false)
		{
			GameInput.setKey(num,-1);
		}
	}
	public static void changeKeyPressed(int num)
	{
		if(isPressed(num))
		{
			GameInput.setKey(num,1);
		}
			GameInput.setKey(num,0);
	}
}