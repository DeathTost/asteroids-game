package com.proz.asteroids.controller.input;

import com.proz.asteroids.controller.GameInput;

abstract public class XboxKeys implements GameKeys {

	public static final int BUTTON_A=0; //3
	public static final int BUTTON_B=1;
	public static final int BUTTON_X=2;
	public static final int BUTTON_Y=3;
	public static final int BUTTON_LB=4;
	public static final int BUTTON_RB=5;
	public static final int BUTTON_BACK=6;
	public static final int BUTTON_START=7; //4
	public static final int BUTTON_LS=8;
	public static final int BUTTON_RS=9; //0
	
	public static final int POV=0;
	public static final int AXIS_LY=0;
	public static final int AXIS_LX=1;
	public static final int AXIS_RY=2;
	public static final int AXIS_RX=3;
	public static final int AXIS_TRIGGER=4;
	private static boolean[] keys=new boolean[NUM_KEYS];
	private static boolean[] pkeys=new boolean[NUM_KEYS];
	private static GameInput GameInput;
	
	public static void update()
	{
		changeKeyPressed(GameKeys.ATTACK);
		for(int i=0;i<NUM_KEYS;i++)
		{
			changeKey(i);
			pkeys[i]=keys[i];
		}
	}
	public static void setGameInput(GameInput GI)
	{
		GameInput=GI;
	}
	public static void setKey(int k, boolean b)
	{
		keys[k]=b;
	}
	public static boolean isDown(int k)
	{
		return keys[k];
	}
	public static boolean isPressed(int k)
	{
		return keys[k] && !pkeys[k];
	}
	public static void changeKey(int num)
	{
		if(pkeys[num]==false && keys[num]==true)
		{
			GameInput.setKey(num,1);
		}
		if(pkeys[num]==true && keys[num]==false)
		{
			GameInput.setKey(num,-1);
		}
	}
	public static void changeKeyPressed(int num)
	{
		if(isPressed(num))
		{
			GameInput.setKey(num,1);
		}
			GameInput.setKey(num,0);
	}

}