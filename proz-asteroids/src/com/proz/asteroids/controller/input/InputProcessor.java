package com.proz.asteroids.controller.input;

import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;
import com.proz.asteroids.controller.GameInput;

public class InputProcessor extends InputAdapter {

	public InputProcessor(GameInput g_in)
	{
		DesktopKeys.setGameInput(g_in);
	}
	
	public boolean keyDown(int k)
	{	
		if(k == Keys.UP)
		{
			DesktopKeys.setKey(GameKeys.THRUST, true);
			DesktopKeys.setKey(GameKeys.UP, true);
		}
		else if(k == Keys.DOWN)
		{
			DesktopKeys.setKey(GameKeys.DOWN, true);
		}
		else if(k == Keys.LEFT)
		{
			DesktopKeys.setKey(GameKeys.LEFT_ROT, true);
		}
		else if(k == Keys.RIGHT)
		{
			DesktopKeys.setKey(GameKeys.RIGHT_ROT, true);
		}
		else if(k == Keys.SPACE)
		{
			DesktopKeys.setKey(GameKeys.ATTACK, true);
		}
		else if(k == Keys.ESCAPE)
		{
			DesktopKeys.setKey(GameKeys.EXIT, true);
		}
		else if(k == Keys.ENTER)
		{
			DesktopKeys.setKey(GameKeys.ACCEPT, true);
		}
		else if(k == Keys.R)
		{
			DesktopKeys.setKey(GameKeys.RESTART, true);
		}
		return true;
	}
	
	public boolean keyUp(int k)
	{
		if(k == Keys.UP)
		{
			DesktopKeys.setKey(GameKeys.THRUST, false);
			DesktopKeys.setKey(GameKeys.UP, false);
		}
		else if(k == Keys.DOWN)
		{
			DesktopKeys.setKey(GameKeys.DOWN, false);
		}
		else if(k == Keys.LEFT)
		{
			DesktopKeys.setKey(GameKeys.LEFT_ROT, false);
		}
		else if(k == Keys.RIGHT)
		{
			DesktopKeys.setKey(GameKeys.RIGHT_ROT, false);
		}
		else if(k == Keys.SPACE)
		{
			DesktopKeys.setKey(GameKeys.ATTACK, false);
		}
		else if(k == Keys.ESCAPE)
		{
			DesktopKeys.setKey(GameKeys.EXIT, false);
		}
		else if(k == Keys.ENTER)
		{
			DesktopKeys.setKey(GameKeys.ACCEPT, false);
		}
		else if(k == Keys.R)
		{
			DesktopKeys.setKey(GameKeys.RESTART, false);
		}
		return true;
	}
	
	public void update()
	{
		DesktopKeys.update();
	}
}