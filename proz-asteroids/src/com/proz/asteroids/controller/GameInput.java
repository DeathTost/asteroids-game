package com.proz.asteroids.controller;

import com.proz.asteroids.controller.input.GameKeys;

public class GameInput {
	
	private int[] keys;
	
	public GameInput()
	{
		keys=new int[GameKeys.NUM_KEYS];
	}
	public void clearInput()
	{
		for(int i=0;i<GameKeys.NUM_KEYS;i++)
		{
			keys[i]=0;
		}
	}
	public int getKey(int i)
	{
		return keys[i];
	}
	public void setKey(int num,int val)
	{
		keys[num]+=val;
		if(keys[num] < 0)
		{
			keys[num]=0;
		}
	}
	private boolean checkKeyValue(int num)
	{
		if(keys[num]>0)
		{
			return true;
		}
		return false;
	}
	private boolean checkKeyValueBoolean(int num)
	{
		if(keys[num]>0)
		{
			keys[num]=0;
			return true;
		}
		return false;
	}
	public boolean isLeftMoveIndicated()
	{
		return checkKeyValue(GameKeys.LEFT_ROT);
	}
	public boolean isRightMoveIndicated()
	{
		return checkKeyValue(GameKeys.RIGHT_ROT);
	}
	public boolean isForwardMoveIndicated()
	{
		return checkKeyValue(GameKeys.THRUST);
	}
	public boolean isAttackIndicated()
	{
		return checkKeyValueBoolean(GameKeys.ATTACK);
	}
	public boolean isExitIndicated()
	{
		return checkKeyValue(GameKeys.EXIT);
	}
	public boolean isUpIndicated()
	{
		return checkKeyValue(GameKeys.UP);
	}
	public boolean isDownIndicated()
	{
		return checkKeyValue(GameKeys.DOWN);
	}
	public boolean isAccepted()
	{
		return checkKeyValue(GameKeys.ACCEPT);
	}
	public boolean isRestarted()
	{
		return checkKeyValue(GameKeys.RESTART);
	}
}