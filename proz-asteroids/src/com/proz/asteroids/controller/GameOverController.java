package com.proz.asteroids.controller;

import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;

public class GameOverController {
	private GameInput GameInput;
	private InputProcessor InputProcessor;
	private ControllerInput ControllerInput;
	private GameStateController gsc;
	
	public GameOverController(GameStateController gsc,InputProcessor InputProcessor,ControllerInput ControllerInput,GameInput GameInput)
	{	
		this.gsc=gsc;
		this.GameInput=GameInput;
		this.InputProcessor=InputProcessor;
		this.ControllerInput=ControllerInput;
		this.GameInput.clearInput();
	}
	public void update(float dt)
	{
		processInput();
		InputProcessor.update();
		if(ControllerInput != null)
		{
			ControllerInput.update();
		}
	}
	
	private void processInput()
	{
		if(GameInput.isRestarted())
		{
			gsc.setState(GameStateController.PLAY);
		}
		if(GameInput.isExitIndicated() || GameInput.isAccepted())
		{
			gsc.setState(GameStateController.MENU);
		}
	}	
}