package com.proz.asteroids.controller;

import com.proz.asteroids.controller.input.ControllerInput;
import com.proz.asteroids.controller.input.InputProcessor;
import com.proz.asteroids.model.World;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.utils.Array;

public class WorldController {

	private World world;
	private GameInput GameInput;
	private InputProcessor InputProcessor;
	private ControllerInput ControllerInput;
	private GameStateController gsc;
	Array<Controller> controllers=Controllers.getControllers();
	
	public WorldController(GameStateController gsc,World world,InputProcessor InputProcessor,ControllerInput ControllerInput,GameInput GameInput)
	{
		this.gsc=gsc;
		this.world=world;
		this.GameInput=GameInput;
		this.InputProcessor=InputProcessor;
		this.ControllerInput=ControllerInput;
	}
	public void update(float dt)
	{	
		processInput();
		world.update(dt);
		InputProcessor.update();
		if(ControllerInput != null)
		{
			ControllerInput.update();
		}
		if(world.getPlayer().getLifes() < 0)
		{
			setGameOver(world);
		}
	}
	
	private void processInput()
	{
		if(!world.getPlayer().isHit())
		{
			world.getPlayer().setLeftRotationEnabled(GameInput.isLeftMoveIndicated());
			world.getPlayer().setRightRotationEnabled(GameInput.isRightMoveIndicated());
			world.getPlayer().setForwardMovementEnabled(GameInput.isForwardMoveIndicated());
			if(GameInput.isAttackIndicated())
			{
				world.getPlayer().shoot();
			}
		}
		if(GameInput.isExitIndicated())
		{
			gsc.setState(GameStateController.MENU);
		}
	}
	private void setGameOver(World world)
	{
		gsc.setState(GameStateController.GAMEOVER);
	}
}
