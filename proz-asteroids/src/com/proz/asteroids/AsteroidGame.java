package com.proz.asteroids;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.proz.asteroids.controller.GameStateController;

public class AsteroidGame extends ApplicationAdapter {
	public static final String Version="Asteroids 0.6.1 Release Candidate";
	public static int WIDTH;
	public static int HEIGHT;
	public static OrthographicCamera cam;
	
	private GameStateController gsc;
	
	@Override
	public void create() {
		WIDTH=Gdx.graphics.getWidth();
		HEIGHT=Gdx.graphics.getHeight();
		cam = new OrthographicCamera(WIDTH,HEIGHT);
		cam.translate(WIDTH/2, HEIGHT/2,0);
		cam.update();
		
		gsc = new GameStateController(WIDTH,HEIGHT);
	}
	
	public void render()
	{
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		gsc.update(Gdx.graphics.getDeltaTime());
		gsc.render();
	}


	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}

	@Override
	public void dispose() {
		super.dispose();
	}

}
