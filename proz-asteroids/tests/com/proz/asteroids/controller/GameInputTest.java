package com.proz.asteroids.controller;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.controller.GameInput;
import com.proz.asteroids.controller.input.GameKeys;
import com.proz.asteroids.model.World;

public class GameInputTest {

	GameInput g_in;
	World world;
	
	@Before
	public void initializeObjects()
	{
		AsteroidGame.WIDTH=400;
		AsteroidGame.HEIGHT=400;
		g_in = new GameInput();
		world = new World(AsteroidGame.WIDTH,AsteroidGame.HEIGHT);
		world.createWorld();
	}
	@Test
	public void testGameInputKeysAreZero() 
	{
		for(int i=0;i<GameKeys.NUM_KEYS;i++)
		{
			assertEquals("Key of i is equal to 0",0,g_in.getKey(i),0);
		}
	}

	@Test
	public void testClearInputKeys() 
	{
		for(int i=0;i<GameKeys.NUM_KEYS;i++)
		{
			g_in.setKey(i, i+1);
			assertNotEquals("Key of i is not equal to 0",0,g_in.getKey(i),0);
		}
		g_in.clearInput();
		for(int i=0;i<GameKeys.NUM_KEYS;i++)
		{
			assertEquals("Key of i is equal to 0",0,g_in.getKey(i),0);
		}	
	}

	@Test
	public void testSetKeyToValue() 
	{
		g_in.setKey(0, 0);
		g_in.setKey(1, 100);
		g_in.setKey(2, -100);
		assertEquals("Key 0 is equal to 0",0,g_in.getKey(0),0);
		assertEquals("Key 1 is equal to 100",100,g_in.getKey(1),0);
		assertEquals("Key 2 is equal to 0 because key value cannot be lower than 0",0,g_in.getKey(2),0);
	}

	@Test
	public void testPlayerRotatedLeftAfterKeyIndacation() 
	{
		g_in.setKey(GameKeys.LEFT_ROT, 1);
		world.getPlayer().setLeftRotationEnabled(g_in.isLeftMoveIndicated());
		world.getPlayer().update(1.0f);
		
		assertEquals("Player should rotate left calculated radians",4.57075,world.getPlayer().getRadians(),0.01);
	}

	@Test
	public void testPlayerRotatedRightAfterKeyIndication() 
	{
		g_in.setKey(GameKeys.RIGHT_ROT, 1);
		world.getPlayer().setRightRotationEnabled(g_in.isRightMoveIndicated());
		world.getPlayer().update(1.0f);
		
		assertEquals("Player should rotate right calculated radians",-1.42925,world.getPlayer().getRadians(),0.01);
	}
	@Test
	public void testPlayerDontRotateBothSidesIndication() 
	{
		g_in.setKey(GameKeys.LEFT_ROT, 1);
		g_in.setKey(GameKeys.RIGHT_ROT, 1);
		world.getPlayer().setLeftRotationEnabled(g_in.isLeftMoveIndicated());
		world.getPlayer().setRightRotationEnabled(g_in.isRightMoveIndicated());
		world.getPlayer().update(1.0f);
		
		assertEquals("Player shouldnt rotate beacause both rotations are enabled",1.57075,world.getPlayer().getRadians(),0.01);
	}
	@Test
	public void testPlayerDontChangePositionAfterRotationIndication() 
	{
		g_in.setKey(GameKeys.LEFT_ROT, 1);
		world.getPlayer().setLeftRotationEnabled(g_in.isLeftMoveIndicated());
		world.getPlayer().update(1.0f);
		assertEquals("Player position x shouldnt change",200.0,world.getPlayer().getPosition().x,0);
		assertEquals("Player position y shouldnt change",200.0,world.getPlayer().getPosition().y,0);
	}
	
	@Test
	public void testPlayerChangePositionAfterForwardMoveIndication() 
	{
		g_in.setKey(GameKeys.THRUST, 1);
		world.getPlayer().setForwardMovementEnabled(g_in.isForwardMoveIndicated());
		world.update(1.0f);
		
		assertNotEquals("Player position x has changed",200.0,world.getPlayer().getPosition().x,0);
		assertNotEquals("Player position y has changed",200.0,world.getPlayer().getPosition().y,0);
	}
	
	@Test
	public void testPlayerDontChangeRotationAfterForwardMoveIndication() 
	{
		g_in.setKey(GameKeys.THRUST, 1);
		world.getPlayer().setForwardMovementEnabled(g_in.isForwardMoveIndicated());
		world.update(1.0f);
		
		assertEquals("Player rotation shouldnt change", 1.57075, world.getPlayer().getRadians(),0.01);
	}

	@Test
	public void testIsAttackIndicated() 
	{
		g_in.setKey(GameKeys.ATTACK, 1);
		world.getPlayer().shoot();
		assertTrue("Player is shooting",g_in.isAttackIndicated());
	}
	@Test
	public void testBulletsAreUsedDuringShooting() 
	{
		g_in.setKey(GameKeys.ATTACK, 1);
		world.getPlayer().shoot();
		assertFalse("Bullets arraylist is not empty",world.getPlayer().getPlayerBullets().isEmpty());
		assertEquals("One bullet has been shot",1,world.getBullet().size(),0);
	}
	
	@Test
	public void testIsShootingIndicatedDisabled() 
	{
		assertEquals("Shooting is disabled",0,g_in.getKey(GameKeys.ATTACK),0);
	}
	@Test
	public void testIsShootingIndicatedEnabled() 
	{
		g_in.setKey(GameKeys.ATTACK, 1);
		assertEquals("Shooting is enabled",1,g_in.getKey(GameKeys.ATTACK),0);
	}
	
	@Test
	public void testIsForwardMoveIndicatedDisabled() 
	{
		assertEquals("Forward movement is disabled",0,g_in.getKey(GameKeys.THRUST),0);
	}
	@Test
	public void testIsForwardMoveIndicatedEnabled() 
	{
		g_in.setKey(GameKeys.THRUST, 1);
		assertEquals("Forward movement is enabled",1,g_in.getKey(GameKeys.THRUST),0);
	}
	
	@Test
	public void testIsLeftMoveIndicatedDisabled() 
	{
		assertEquals("Left rotation is disabled",0,g_in.getKey(GameKeys.LEFT_ROT),0);
	}
	@Test
	public void testIsLeftMoveIndicatedEnabled() 
	{
		g_in.setKey(GameKeys.LEFT_ROT, 1);
		assertEquals("Left rotation is enabled",1,g_in.getKey(GameKeys.LEFT_ROT),0);
	}
	@Test
	public void testIsRightMoveIndicatedDisabled() 
	{
		assertEquals("Right rotation is disabled",0,g_in.getKey(GameKeys.RIGHT_ROT),0);
	}
	@Test
	public void testIsRightMoveIndicatedEnabled() 
	{
		g_in.setKey(GameKeys.RIGHT_ROT, 1);
		assertEquals("Right rotation is enabled",1,g_in.getKey(GameKeys.RIGHT_ROT),0);
	}
	
	@Test
	public void testIsExitIndicatedDisabled() 
	{
		assertEquals("Exit is disabled",0,g_in.getKey(GameKeys.EXIT),0);
	}
	@Test
	public void testIsExitIndicatedEnabled() 
	{
		g_in.setKey(GameKeys.EXIT, 1);
		assertEquals("Exit is enabled",1,g_in.getKey(GameKeys.EXIT),0);
	}

	@Test
	public void testIsUpIndicatedDisabled() 
	{
		assertEquals("UpIndicator is disabled",0,g_in.getKey(GameKeys.UP),0);
	}
	@Test
	public void testIsUpIndicatedEnabled() 
	{
		g_in.setKey(GameKeys.UP, 1);
		assertEquals("UpIndicator is enabled",1,g_in.getKey(GameKeys.UP),0);
	}

	@Test
	public void testIsDownIndicatedDisabled() 
	{
		assertEquals("DownIndicator is disabled",0,g_in.getKey(GameKeys.DOWN),0);
	}
	@Test
	public void testIsDownIndicatedEnabled() 
	{
		g_in.setKey(GameKeys.DOWN, 1);
		assertEquals("DownIndictor is enabled",1,g_in.getKey(GameKeys.DOWN),0);
	}

	@Test
	public void testIsAcceptedDisabled() 
	{
		assertEquals("AcceptIndicator is disabled",0,g_in.getKey(GameKeys.ACCEPT),0);
	}
	@Test
	public void testIsAcceptedEnabled() 
	{
		g_in.setKey(GameKeys.ACCEPT, 1);
		assertEquals("AcceptIndicator is enabled",1,g_in.getKey(GameKeys.ACCEPT),0);
	}

	@Test
	public void testIsRestartedDisabled() 
	{
		assertEquals("RestartIndicator is disabled",0,g_in.getKey(GameKeys.RESTART),0);
	}
	@Test
	public void testIsRestartedEnabled() 
	{
		g_in.setKey(GameKeys.RESTART, 1);
		assertEquals("RestartIndicator is enabled",1,g_in.getKey(GameKeys.RESTART),0);
	}
}
