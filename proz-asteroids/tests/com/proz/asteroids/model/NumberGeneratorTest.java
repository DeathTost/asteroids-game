package com.proz.asteroids.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.proz.asteroids.model.numbergenerator.*;

public class NumberGeneratorTest {

	RandomNumberGenerator RandGen;
	ConstantNumberGenerator ConstGen;
	
	@Before
	public void initializeObjects()
	{
		RandGen = new RandomNumberGenerator();
		ConstGen = new ConstantNumberGenerator();
	}
	
	@Test
	public void testConstantGetIntValueFromRange() 
	{
		assertEquals("Constant generated value should be equal to start value=0",0,ConstGen.getIntValueFromRange(0, Integer.MAX_VALUE),0);
		assertEquals("Constant generated value should be equal to start value=Integer.MIN_VALUE",Integer.MIN_VALUE,ConstGen.getIntValueFromRange(Integer.MIN_VALUE, 0),0);
		assertEquals("Constant generated value should be equal to start value=4",4,ConstGen.getIntValueFromRange(4,4),0);
		assertEquals("Constant generated value should be equal to start value=5, the order doesnt matter",5,ConstGen.getIntValueFromRange(5, 1),0);
	}
	
	/*	probability tests pointless because RandomNumberGenerator is Java.MathUtils.random() implementation
	@Test
	public void testRandomGetIntValueFromRange() 
	{
	}
	@Test
	public void testGetRandomFloatValue() 
	{
	}
	*/
	
	@Test
	public void testGetConstantFloatValue() 
	{
		assertEquals("Constant generated value should be equal to range value=0",0,ConstGen.getFloatValue(0),0.01);
		assertEquals("Constant generated value should be equal to range value=Float.POSITIVE_INFINITY",Float.POSITIVE_INFINITY,ConstGen.getFloatValue(Float.POSITIVE_INFINITY),0.01);
		assertEquals("Constant generated value should be equal to range value=Float.MIN_VALUE",Float.MIN_VALUE,ConstGen.getFloatValue(Float.MIN_VALUE),0.01);
	}

}
