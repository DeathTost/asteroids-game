package com.proz.asteroids.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.Asteroid;
import com.proz.asteroids.model.Bullet;
import com.proz.asteroids.model.Particle;
import com.proz.asteroids.model.Player;
import com.proz.asteroids.model.numbergenerator.*;

public class SpaceObjectTest {
	
	static NumberGenerator NumGen;
	Asteroid ast0;
	Asteroid ast1;
	Asteroid ast2;
	Asteroid astZZ;
	Asteroid astMVMV;
	Asteroid astNFNF;
	Bullet bul0;
	Bullet bul1;
	Bullet bul2;
	Bullet bul3;
	Bullet bulMVMV;
	Bullet bulNFNF;
	Player plyr0;
	Player plyr1;
	Player plyr2;
	Particle partMVMV;
	Particle partNFNF;
	
	@BeforeClass
	public static void initializeWorld()
	{
		AsteroidGame.WIDTH=400;
		AsteroidGame.HEIGHT=400;
		NumGen = new ConstantNumberGenerator();
	}
	@Before
	public void initializeObjects()
	{
		ast0 = new Asteroid(100,100,0,NumGen);
		ast1 = new Asteroid(100,100,0,NumGen);
	    ast2 = new Asteroid(200,200,0,NumGen);
	    astZZ = new Asteroid(0,0,0,NumGen);
		bul1 = new Bullet(100,100,0);
		bul0 = new Bullet(0,0,0);
		bul2 = new Bullet(200,200,0);
		bul3 = new Bullet(200,200,0);
		plyr0= new Player(null);
		plyr1= new Player(null);
		plyr2= new Player(null);
		astMVMV=new Asteroid(Float.MAX_VALUE,Float.MAX_VALUE,0,NumGen);
		astNFNF=new Asteroid(Float.NEGATIVE_INFINITY,Float.NEGATIVE_INFINITY,0,NumGen);
		bulMVMV = new Bullet(Float.MAX_VALUE,Float.MAX_VALUE,0);
		bulNFNF=new Bullet(Float.NEGATIVE_INFINITY,Float.NEGATIVE_INFINITY,0);
		partMVMV = new Particle(Float.MAX_VALUE,Float.MAX_VALUE,NumGen);
		partNFNF = new Particle(Float.NEGATIVE_INFINITY,Float.NEGATIVE_INFINITY,NumGen);
	}
	
	@Test
	public void testContainsPointInAsteroid() 
	{	
		assertTrue("Point should be inside asteroid",ast0.contains(100, 100));
		assertFalse("Point should be outside asteroid",ast0.contains(0, 0));
	}
	
	@Test
	public void testContainsBulletInAsteroid() 
	{	
		assertTrue("Bullet1 should be inside asteroid",ast0.contains(bul1.getPosition().x, bul1.getPosition().y));
		assertFalse("Bulet0 should be outside asteroid",ast0.contains(bul0.getPosition().x, bul0.getPosition().y));	
	}
	@Test
	public void testIntersectsAsteroidWithAsteroid() 
	{
		assertTrue("Asteroid0 should intersect with asteroid1",ast0.intersects(ast1));
		assertFalse("Asteroid0 shouldnt intersect with asteroid2",ast0.intersects(ast2));
		assertFalse("Asteroid0 shouldnt intersect with asteroid0",ast0.intersects(ast0));
	}

	@Test
	public void testContainsPointInPlayer() 
	{
		assertTrue("Point should be inside player",plyr0.contains(200, 200));
		assertFalse("Point should be outside player",plyr0.contains(0, 0));
	}
	
	@Test
	public void testContainsPointInBullet() 
	{
		assertFalse("Point cannot be inside bullet because bullet is a point",bul1.contains(100.0f, 100.0f));
		assertFalse("Point cannot be outside bullet because bullet is a point",bul1.contains(0, 0));
	}

	@Test
	public void testIntersectsAsteroidWithBullet() 
	{
		assertFalse("Asteroid cannot intersect with bul1",bul1.intersects(ast0));
		assertFalse("Bullet1 shouldnt intersect with asteroid",ast0.intersects(bul1));
		assertFalse("Bullet0 shouldnt intersect with asteroid",ast0.intersects(bul0));
		assertFalse("Asteroid cannot intersect with bul0",bul0.intersects(ast0));
	}
	
	@Test
	public void testIntersectsAsteroidWithPlayer() 
	{
		assertTrue("Asteroid0 should intersect with player",plyr0.intersects(ast2));
		assertTrue("Player should intersect with asteroid0",ast2.intersects(plyr0));
		assertFalse("AsteroidZZ shouldnt intersect with player",plyr0.intersects(astZZ));
		assertFalse("Player shouldnt intersect with asteroidZZ",astZZ.intersects(plyr0));
	}
	@Test
	public void testIntersectsBulletWithPlayer() 
	{
		plyr0.setPosition(200,200);

		assertFalse("Player cannot intersect with bul0",bul0.intersects(plyr0));
		assertFalse("Bullet0 shouldnt intersect with player",plyr0.intersects(bul0));
		assertFalse("Bullet1 shouldnt intersect with player",plyr0.intersects(bul2));
		assertFalse("Player cannot intersect with bul1",bul2.intersects(plyr0));
	}
	@Test
	public void testContainsBulletInPlayer() 
	{
		plyr0.setPosition(200,200);
		
		assertTrue("Bullet0 should be inside player",plyr0.contains(bul2.getPosition().x, bul2.getPosition().y));
		assertFalse("Bulet1 should be outside player",plyr0.contains(bul0.getPosition().x, bul0.getPosition().y));
	}
	@Test
	public void testIntersectsPlayerWithPlayer() 
	{
		plyr0.setPosition(200,200);
		plyr1.setPosition(200,200);
		plyr2.setPosition(0,0);
		
		assertTrue("Player0 should intersect with player1",plyr1.intersects(plyr0));
		assertTrue("Player1 should intersect with player0",plyr0.intersects(plyr1));
		assertFalse("Plater0 shouldnt intersect with player2",plyr0.intersects(plyr2));
		assertFalse("Player2 shouldnt intersect with player0",plyr2.intersects(plyr0));
	}
	@Test
	public void testIntersectsBulletWithBullet() 
	{
		assertFalse("Bullet2 cannot intersect with bul3",bul2.intersects(bul3));
		assertFalse("Bullet2 cannot intersect with bul0",bul2.intersects(bul0));
		assertFalse("Bullet3 cannot intersect with bul2",bul3.intersects(bul2));
		assertFalse("Bullet0 cannot intersect with bul2",bul0.intersects(bul2));
	}
	
	@Test
	public void testWrapAsteroidPosition() 
	{
		astMVMV.wrap(astMVMV);
		astNFNF.wrap(astNFNF);
		assertEquals("AsteroidMVMV has set position x=MAX_VALUE after wrap should be x=0 is x=0", 0,astMVMV.getPosition().x,0);
		assertEquals("AsteroidMVMV has set position y=MAX_VALUE after wrap should be y=0 is y=0", 0,astMVMV.getPosition().y,0);
		assertEquals("AsteroidNFNF has set position x=NEGATIVE_INFINITY after wrap should be x=400 is x=400", 400,astNFNF.getPosition().x,0);
		assertEquals("AsteroidNFNF has set position y=NEGATIVE_INFINITY after wrap should be y=400 is y=400", 400,astNFNF.getPosition().y,0);
	}
	@Test
	public void testWrapBulletPosition() 
	{
		bulMVMV.wrap(bulMVMV);
		bulNFNF.wrap(bulNFNF);
		assertEquals("BulletMVMV has set position x=MAX_VALUE after wrap should be x=0 is x=0", 0,bulMVMV.getPosition().x,0);
		assertEquals("BulletMVMV has set position y=MAX_VALUE after wrap should be y=0 is y=0", 0,bulMVMV.getPosition().y,0);
		assertEquals("BulletNFNF has set position x=NEGATIVE_INFINITY after wrap should be x=400 is x=400", 400,bulNFNF.getPosition().x,0);
		assertEquals("BulletNFNF has set position y=NEGATIVE_INFINITY after wrap should be y=400 is y=400", 400,bulNFNF.getPosition().y,0);
	}
	@Test
	public void testWrapPlayerPosition() 
	{
		plyr0.setPosition(Float.MAX_VALUE,Float.MAX_VALUE);
		plyr1.setPosition(Float.NEGATIVE_INFINITY,Float.NEGATIVE_INFINITY);
		plyr0.wrap(plyr0);
		plyr1.wrap(plyr1);
		
		assertEquals("Player0 has set position x=MAX_VALUE after wrap should be x=0 is x=0", 0,plyr0.getPosition().x,0);
		assertEquals("Player0 has set position y=MAX_VALUE after wrap should be y=0 is y=0", 0,plyr0.getPosition().y,0);
		assertEquals("Player1 has set position x=NEGATIVE_INFINITY after wrap should be x=400 is x=400", 400,plyr1.getPosition().x,0);
		assertEquals("Player1 has set position y=NEGATIVE_INFINITY after wrap should be y=400 is y=400", 400,plyr1.getPosition().y,0);
	}

	@Test
	public void testWrapParticlePosition() 
	{
		partMVMV.wrap(partMVMV);
		partNFNF.wrap(partNFNF);
		assertEquals("ParticleMVMV has set position x=MAX_VALUE after wrap should be x=0 is x=0", 0,partMVMV.getPosition().x,0);
		assertEquals("ParticleMVMV has set position y=MAX_VALUE after wrap should be y=0 is y=0", 0,partMVMV.getPosition().y,0);		
		assertEquals("ParticleNFNF has set position x=NEGATIVE_INFINITY after wrap should be x=400 is x=400", 400,partNFNF.getPosition().x,0);
		assertEquals("ParticleNFNF has set position y=NEGATIVE_INFINITY after wrap should be y=400 is y=400", 400,partNFNF.getPosition().y,0);	
	}
}