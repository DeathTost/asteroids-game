package com.proz.asteroids.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.proz.asteroids.model.GameOverProgressData;

public class GameOverProgressDataTest {

	GameOverProgressData gopd;
	GameOverProgressData gopd1;
	
	@Before
	public void initializeObejcts()
	{
		gopd = new GameOverProgressData();
		gopd1 = new GameOverProgressData();
	}
	@Test
	public void testSetFinalScore() 
	{	
		gopd.setFinalData(Integer.MIN_VALUE,1);
		gopd1.setFinalData(Long.MAX_VALUE, 1);
		
		assertEquals("Minimal score number is 0", 0,gopd.getFinalScore());
		assertEquals("Maximal score number is Long.MAX_VALUE", Long.MAX_VALUE,gopd1.getFinalScore(),0);
	}
	@Test
	public void testSetFinalLevel() 
	{	
		gopd.setFinalData(0,Integer.MIN_VALUE);
		gopd1.setFinalData(0, Integer.MAX_VALUE);
		
		assertEquals("Minimal level numer is 1", 1,gopd.getFinalLevel());
		assertEquals("Maximal level numer is Integer.MAX_VALUE", Integer.MAX_VALUE,gopd1.getFinalLevel());
	}
}