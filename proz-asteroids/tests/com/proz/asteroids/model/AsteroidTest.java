package com.proz.asteroids.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.Asteroid;
import com.proz.asteroids.model.numbergenerator.*;

public class AsteroidTest {

	Asteroid ast0;
	Asteroid ast1;
	Asteroid ast2;
	Asteroid ast999;
	Asteroid ast_12;
	Asteroid ast1kX;
	Asteroid ast1kY;
	Asteroid ast4kX4kY;
	static NumberGenerator NumGen;
	
	@BeforeClass
	public static void initializeWorld()
	{
		AsteroidGame.WIDTH=400;
		AsteroidGame.HEIGHT=400;
		NumGen = new ConstantNumberGenerator();
	}
	@Before
	public void initializeObjects()
	{
		ast0=new Asteroid(10,10,0,NumGen);
		ast1=new Asteroid(10,10,1,NumGen);
		ast2=new Asteroid(10,10,2,NumGen);
		ast999=new Asteroid(10,10,999,NumGen);
		ast_12=new Asteroid(10,10,-12,NumGen);
		ast1kX=new Asteroid(10000,10,0,NumGen);
		ast1kY=new Asteroid(10,110000,0,NumGen);
		ast4kX4kY=new Asteroid(-40000,-40000,2,NumGen);
	}
	
	@Test
	public void testGetType() 
	{
		assertEquals("Asteroid0 set type 0 has type=0", 0,ast0.getType());
		assertEquals("Asteroid1 set type 1 has type=1", 1,ast1.getType());
		assertEquals("Asteroid2 set type 2 has type=2", 2,ast2.getType());
		assertEquals("Asteroid999 set type 999 has type=2", 2,ast999.getType());
		assertEquals("Asteroid_12 set type -12 has type=0", 0,ast_12.getType());
	}
	@Test
	public void testGetRadius() 
	{
		assertEquals("Asteroid0 has type=0 so has radius=10", 10,ast0.getRadius());
		assertEquals("Asteroid1 has type=1 so has radius=22", 22,ast1.getRadius());
		assertEquals("Asteroid2 has type=2 so has radius=40", 40,ast2.getRadius());
		assertEquals("Asteroid999 has type 999 which is equal to type=2 so has radius=40", 40,ast999.getRadius());
		assertEquals("Asteroid_12 has type -12 which is equal to type=0 so has radius=10", 10,ast_12.getRadius());
	}

	@Test
	public void testUpdatePosition() 
	{		
		ast0.update(1.0f);
		ast4kX4kY.update(1.0f);

		assertEquals("Asteroid0 after update should have position x=80.0 is equal to calculated x",80.0,ast0.getPosition().x,0.01);
		assertEquals("Asteroid0 after update should have position y=80.0 is equal to calculated y", 9.987,ast0.getPosition().y,0.01);		
		assertEquals("Asteroid2 after update should have position x=400.0 is equal to calculated x",400.0,ast4kX4kY.getPosition().x,0.01);
		assertEquals("Asteroid2 after update should have position y=400.0 is equal to calculated y", 400.0,ast4kX4kY.getPosition().y,0.01);
	}
	
	@Test
	public void testUpdateRadians() 
	{		
		ast0.update(1.0f);
		ast4kX4kY.update(1.0f);

		assertEquals("Asteroid0 after update should have radians=5.283 are equal to calculated radians", 5.283,ast0.getRadians(),0.01);
		assertEquals("Asteroid2 after update should have radians=5.283 are equal to calculated radians", 5.283,ast4kX4kY.getRadians(),0.01);
	}

	@Test
	public void testGetScore() 
	{
		assertEquals("Asteroid SMALL has score=100", 100,ast0.getScore());
		assertEquals("Asteroid MEDIUM has score=50", 50,ast1.getScore());
		assertEquals("Asteroid BIG has score=20", 20,ast2.getScore());
		assertEquals("Asteroid set type 999 is BIG has score=20", 20,ast999.getScore());
		assertEquals("Asteroid set type -12 is SMALL has score=100", 100,ast_12.getScore());
	}

	@Test
	public void testSetPositionInConstructor() 
	{
		ast0.update(0.0f);
		ast1kX.update(0.0f);
		ast1kY.update(0.0f);
		ast4kX4kY.update(0.0f);
		
		assertEquals("Asteroid has position x=10 is x=10", 10,ast0.getPosition().x,0);
		assertEquals("Asteroid has position y=10 is y=10", 10,ast0.getPosition().y,0);
		assertEquals("Asteroid has position x=10000 because of wrap is x=0", 0,ast1kX.getPosition().x,0);
		assertEquals("Asteroid has position y=10 is y=10", 10,ast1kX.getPosition().y,0);
		assertEquals("Asteroid has position x=10 is x=10", 10,ast1kY.getPosition().x,0);
		assertEquals("Asteroid has position y=110000 because of wrap is y=0", 0,ast1kY.getPosition().y,0);
		assertEquals("Asteroid has position x=-40000 because of wrap is x=400", 400,ast4kX4kY.getPosition().x,0);
		assertEquals("Asteroid has position y=-40000 because of wrap is y=400", 400,ast4kX4kY.getPosition().y,0);
	}
	
	@Test
	public void testSetPositionVector2AfterUpdate()
	{
		ast0.update(0.0f);
		ast1kX.update(0.0f);
		ast1kY.update(0.0f);
		ast4kX4kY.update(0.0f);
		
		ast0.setPosition(25,25);
		ast1kX.setPosition(100,10);
		ast1kY.setPosition(10,10);
		ast4kX4kY.setPosition(1000,1000);
		
		assertEquals("Asteroid0 has set position x=25 is x=25", 25,ast0.getPosition().x,0);
		assertEquals("Asteroid0 has set position y=25 is y=25", 25,ast0.getPosition().y,0);
		assertEquals("Asteroid1 has set position x=100 is x=100", 100,ast1kX.getPosition().x,0);
		assertEquals("Asteroid1 has set position y=10 is y=10", 10,ast1kX.getPosition().y,0);
		assertEquals("Asteroid2 has set position x=10 is x=10", 10,ast1kY.getPosition().x,0);
		assertEquals("Asteroid2 has set position y=10 is y=10", 10,ast1kY.getPosition().y,0);
		assertEquals("Asteroid3 has set position x=1000 is x=1000", 1000,ast4kX4kY.getPosition().x,0);
		assertEquals("Asteroid3 has set position y=1000 is y=1000", 1000,ast4kX4kY.getPosition().y,0);
	}
	@Test
	public void testSetPositionVector2BeforeUpdate()
	{
		ast1.setPosition(Float.NEGATIVE_INFINITY, Float.NEGATIVE_INFINITY);
		ast_12.setPosition(Float.MIN_VALUE, Float.MIN_VALUE);
		ast4kX4kY.setPosition(1000,1000);
	
		ast1.update(0.0f);
		ast_12.update(0.0f);
		ast4kX4kY.update(0.0f);

		assertEquals("Asteroid1 has set position x=Float.NEGATIVE_INFINITY after update should be x=400.0", 400.0,ast1.getPosition().x,0.01);
		assertEquals("Asteroid1 has set position y=Float.NEGATIVE_INFINITY after update should be y=400.0", 400.0,ast1.getPosition().y,0.01);
		assertEquals("Asteroid_12 has set position x=Float.MIN_VALUE  after update should be x=Float.MIN_VALUE", Float.MIN_VALUE,ast_12.getPosition().x,0.01);
		assertEquals("Asteroid_12 has set position y=Float.MIN_VALUE  after update should be y=Float.MIN_VALUE", Float.MIN_VALUE,ast_12.getPosition().y,0.01);
		assertEquals("Asteroid3 has set position x=1000 after update should be x=0 ", 0,ast4kX4kY.getPosition().x,0);
		assertEquals("Asteroid3 has set position y=1000 after update should be y=0 ", 0,ast4kX4kY.getPosition().y,0);
	}
}
