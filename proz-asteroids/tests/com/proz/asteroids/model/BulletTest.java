package com.proz.asteroids.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.Bullet;

public class BulletTest {

	Bullet bul0;
	Bullet bul1;
	Bullet bul2;
	Bullet bul3;
	Bullet bul4;
	
	@BeforeClass
	public static void initializeWorld()
	{
		AsteroidGame.WIDTH=400;
		AsteroidGame.HEIGHT=400;
	}
	@Before
	public void initializeObjects()
	{
		bul0 = new Bullet(Float.MAX_VALUE,Float.MAX_VALUE,0.0f);
		bul1 = new Bullet(Float.NEGATIVE_INFINITY,Float.NEGATIVE_INFINITY,0.0f);
		bul2 = new Bullet(0.0f,0.0f,0.0f);
		bul3 = new Bullet(0.0f,0.0f,Float.MIN_VALUE);
		bul4 = new Bullet(0.0f,0.0f,Float.POSITIVE_INFINITY);
	}
	
	@Test
	public void testSetPositionInBulletConstructor() 
	{
		assertEquals("Bullet0 has position x=MAX_VALUE", Float.MAX_VALUE,bul0.getPosition().x,0.01);
		assertEquals("Bullet0 has position y=MAX_VALUE", Float.MAX_VALUE,bul0.getPosition().y,0.01);
		assertEquals("Bullet1 has position x=NEGATIVE_INFINITY", Float.NEGATIVE_INFINITY,bul1.getPosition().x,0.01);
		assertEquals("Bullet1 has position y=NEGATIVE_INFINITY", Float.NEGATIVE_INFINITY,bul1.getPosition().y,0.01);
		assertEquals("Bullet2 has position x=0.0f", 0.0f,bul2.getPosition().x,0.01);
		assertEquals("Bullet2 has position y=0.0f", 0.0f,bul2.getPosition().y,0.01);
	}
	@Test
	public void testSetRadiansInBulletConstructor() 
	{
		assertEquals("Bullet0 has radians equal 0.0f", 0.0f,bul0.getRadians(),0.01);
		assertEquals("Bullet3 has radians equal Float.MIN_VALUE", Float.MIN_VALUE,bul3.getRadians(),0.01);
		assertEquals("Bullet4 has radians equal Float.POSITIVE_INFINITY", Float.POSITIVE_INFINITY,bul4.getRadians(),0.01);
	}
	
	@Test
	public void testShouldRemove() 
	{
		bul0.update(0.0f);
		bul1.update(1.0f);
		bul2.update(2.0f);
	
		assertFalse("Time < Timer.Bullet shouldnt be removed, because time hasnt passed",bul0.shouldRemove());
		assertFalse("Time=Timer.Bullet shouldnt be removed, because time is not greater than timer",bul1.shouldRemove());
		assertTrue("Time>Timer.Bullet must be removed",bul2.shouldRemove());
	}

	@Test
	public void testPositionUpdate() 
	{
		bul2.update(1.0f);
		bul1.update(1.0f);
		bul4.update(1.0f);
		
		assertEquals("Bullet2 has radius 0 so should move only x axis to x=350.0",350.0,bul2.getPosition().x,0.01);
		assertEquals("Bullet2 has radius 0 so should stay y=0", 0.0,bul2.getPosition().y,0.01);
		assertEquals("Bullet1 has position x=Float.NEGATIVE_INFINITY so after update should be x=400.0",400.0,bul1.getPosition().x,0.01);
		assertEquals("Bullet1 has position y=Float.NEGATIVE_INFINITY so after update should be y=400.0", 400.0,bul1.getPosition().y,0.01);
		assertEquals("Bullet4 has radians=Float.POSITIVE_INFINITY so after update x=400.0",400.0,bul4.getPosition().x,0.01);
		assertEquals("Bullet4 has radians=Float.POSITIVE_INFINITY so after update y=400.0", 400.0,bul4.getPosition().y,0.01);
	}
}
