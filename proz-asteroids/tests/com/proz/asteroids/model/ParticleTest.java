package com.proz.asteroids.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.Particle;
import com.proz.asteroids.model.numbergenerator.*;

public class ParticleTest {

	Particle part0;
	Particle part1; 
	Particle part2;
	static NumberGenerator NumGen;

	@BeforeClass
	public static void initializeWorld()
	{
		AsteroidGame.WIDTH=400;
		AsteroidGame.HEIGHT=400;
		NumGen= new ConstantNumberGenerator();
	}
	@Before
	public void initializeObjects()
	{
		part0 = new Particle(Float.MAX_VALUE,Float.MAX_VALUE,NumGen);
		part1 = new Particle(Float.NEGATIVE_INFINITY,Float.NEGATIVE_INFINITY,NumGen);
		part2 = new Particle(100.0f,100.0f,NumGen);
	}
	
	@Test
	public void testsetPositionInParticleConstructor() 
	{
		assertEquals("Particle0 has position x=MAX_VALUE", Float.MAX_VALUE,part0.getPosition().x,0.01);
		assertEquals("Particle0 has position y=MAX_VALUE", Float.MAX_VALUE,part0.getPosition().y,0.01);
		assertEquals("Particle1 has position x=NEGATIVE_INFINITY", Float.NEGATIVE_INFINITY,part1.getPosition().x,0.01);
		assertEquals("Particle1 has position y=NEGATIVE_INFINITY", Float.NEGATIVE_INFINITY,part1.getPosition().y,0.01);
		assertEquals("Particle2 has position x=100.0", 100.0f,part2.getPosition().x,0.01);
		assertEquals("Particle2 has position y=100.0", 100.0f,part2.getPosition().y,0.01);
	}

	@Test
	public void testShouldRemove() 
	{
		part0.update(0.0f);
		part1.update(1.0f);
		part2.update(2.0f);
	
		assertFalse("Time < Timer.Particle shouldnt be removed, because time hasnt passed",part0.shouldRemove());
		assertFalse("Time=Timer.Particle shouldnt be removed, because time is not greater than timer",part1.shouldRemove());
		assertTrue("Time>Timer.Particle must be removed",part2.shouldRemove());
	}

	@Test
	public void testPositionUpdate() 
	{
		part2.update(1.0f);
		part1.update(1.0f);
		
		assertEquals("Particles do not wrap.Particle2 position should be x=150.0f",150.0f,part2.getPosition().x,0.01);
		assertEquals("Particles do not wrap.Particle2 position should be y=99.99f", 99.99f,part2.getPosition().y,0.01);
		assertEquals("Particles do not wrap.Particle1 position should be x=Float.NEGATIVE_INFINITY",Float.NEGATIVE_INFINITY,part1.getPosition().x,0.01);
		assertEquals("Particles do not wrap.Particle1 position should be y=Float.NEGATIVE_INFINITY", Float.NEGATIVE_INFINITY,part1.getPosition().y,0.01);
	}
}