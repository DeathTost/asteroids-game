package com.proz.asteroids.model;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.Asteroid;
import com.proz.asteroids.model.Bullet;
import com.proz.asteroids.model.World;
import com.proz.asteroids.model.numbergenerator.*;

public class WorldTest {

	static NumberGenerator NumGen;
	World world;
	
	@BeforeClass
	public static void initializeWorld()
	{
		AsteroidGame.WIDTH=400;
		AsteroidGame.HEIGHT=400;
		NumGen = new ConstantNumberGenerator();
	}
	@Before
	public void initializeObjects()
	{
		 world = new World(AsteroidGame.WIDTH,AsteroidGame.HEIGHT);
	}
	@Test
	public void testGetBulletFromEmptyWorld() 
	{
		assertEquals("There are no bullets in world, because world wasnt created",null,world.getBullet());
	}
	@Test
	public void testGetEmptyBulletArrayFromCreatedWorld() 
	{
		world.createWorld();
		assertTrue("There is empty bullets array",world.getBullet().isEmpty());
	}
	
	@Test
	public void testGetFullBulletArrayFromCreatedWorld() 
	{
		world.createWorld();
		for(int i=0;i<4;i++)
		{
			world.getBullet().add(new Bullet(0,0,0));
		}
		assertEquals("Bullets array should have 4 bullets",4,world.getBullet().size());
	}

	@Test
	public void testGetPlayerFromEmptyWorld() 
	{
		assertEquals("There is no player",null,world.getPlayer());
	}
	@Test
	public void testGetPlayerFromCreatedWorld() 
	{
		world.createWorld();
		assertFalse("Player exist and dead=false",world.getPlayer().isDead());
		assertEquals("Player should have bullets array",0,world.getPlayer().getPlayerBullets().size(),0);
	}

	@Test
	public void testGetHudPlayerFromEmptyWorld() 
	{	
		assertEquals("There is no playerHud",null,world.getHudPlayer());
	}
	@Test
	public void testGetHudPlayerFromCreatedWorld() 
	{
		world.createWorld();
		assertFalse("PlayerHud exist and dead=false",world.getHudPlayer().isDead());
		assertEquals("PlayerHud shoudnt have bullets array",null,world.getHudPlayer().getPlayerBullets());
	}

	@Test
	public void testGetParticlesFromEmptyWorld() 
	{	
		assertEquals("There are no particles",null,world.getParticles());
	}
	@Test
	public void testGetParticlesFromCreatedWorld() 
	{
		world.createWorld();
		assertTrue("Particles exists, array is empty",world.getParticles().isEmpty());
	}

	@Test
	public void testGetAsteroidsFromEmptyWorld() 
	{
		assertEquals("There are no asteroids in game",null,world.getAsteroids());
	}
	@Test
	public void testGetAsteroidsFromCreatedWorld() 
	{
		world.createWorld();
		assertEquals("Asteroids array exists and has 4 asteroids (4+level-1), level=1",4,world.getAsteroids().size(),0);
	}
	@Test
	public void testGetNoAsteroidsFromClearedWorld() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		assertTrue("All asteroids were destroyed",world.getAsteroids().isEmpty());
	}

	@Test
	public void testGetActualLevelFromEmptyWorld() 
	{
		assertEquals("Level should be 0",0,world.getLevel(),0);
	}
	@Test
	public void testGetActualLevelFromCreatedWorld() 
	{
		world.createWorld();
		assertEquals("World was created, level is 1",1,world.getLevel(),0);
	}
	@Test
	public void testGetActualLevelFromClearedWorld() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.update(0.0f);
		assertEquals("Asteroids were destroyed, level is 2",2,world.getLevel(),0);
		assertEquals("Asteroid number should be 4+level-1=5",5,world.getAsteroids().size(),0);
	}

	@Test
	public void testSpawnAsteroidLessThanZero() 
	{
		world.createMenuAsteroids();
		world.getAsteroids().clear();
		world.spawnAsteroids(Integer.MIN_VALUE);
		assertEquals("NumToSpawn is less than zero so no asteroids will be spawn",0,world.getAsteroids().size(),0);
	}
	@Test
	public void testSpawnAsteroidConstantValue() 
	{
		world.createMenuAsteroids();
		world.getAsteroids().clear();
		world.spawnAsteroids(10);
		assertEquals("There will be 10 asteroids spawn",10,world.getAsteroids().size(),0);
	}
	@Test
	public void testSplitBigAsteroid() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(0,0,2,NumGen));
		world.splitAsteroids(world.getAsteroids().get(0));
		assertEquals("Asteroid type 2=BIG has split to 2 asteroids of type 1=MEDIUM",1,world.getAsteroids().get(1).getType(),0);
		assertEquals("Asteroid type 2=BIG has split to 2 asteroids of type 1=MEDIUM",1,world.getAsteroids().get(2).getType(),0);
	}
	@Test
	public void testSplitMediumAsteroid() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(0,0,1,NumGen));
		world.splitAsteroids(world.getAsteroids().get(0));
		assertEquals("Asteroid type 1=MEDIUM has split to 2 asteroids of type 0=SMALL",0,world.getAsteroids().get(1).getType(),0);
		assertEquals("Asteroid type 1=MEDIUM has split to 2 asteroids of type 0=SMALL",0,world.getAsteroids().get(2).getType(),0);
	}
	@Test
	public void testSplitSmallAsteroid() 
	{	
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(0,0,0,NumGen));
		world.splitAsteroids(world.getAsteroids().get(0));
		int size = world.getAsteroids().size();
		assertEquals("Asteroid of type=SMALL will no split",0,world.getAsteroids().get(0).getType(),0);
		assertEquals("Size of asteroids arraylist hasnt changed",size,world.getAsteroids().size(),0);
	}
	@Test
	public void testSplitWrongTypeAsteroid() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(0,0,99,NumGen));
		world.splitAsteroids(world.getAsteroids().get(0));
		assertEquals("Asteroid has type=99 which is wrong and will be translated to BIG and split to MEDIUM",1,world.getAsteroids().get(1).getType(),0);
	}
	
	@Test
	public void testCreateMenuAsteroids() 
	{
		world.createMenuAsteroids();
		assertEquals("Asteroids array exists and has 10 asteroids",10,world.getAsteroids().size(),0);
	}
	@Test
	public void testPlayerWithoutAsteroidCollision()  
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(0,0,1,NumGen));
		world.PlayerAsteroidCollision();;
		assertFalse("Player hasnt been hit yet",world.getPlayer().isHit());
		assertFalse("Player shouldnt be hit because he doesnt collide",world.getPlayer().isHit());
	}
	@Test
	public void testAsteroidWithoutPlayerCollision()  
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(0,0,1,NumGen));
		int size = world.getAsteroids().size();
		world.PlayerAsteroidCollision();
		assertEquals("There is a MEDIUM asteroid",1,world.getAsteroids().get(0).getType(),0);
		assertNotEquals("Asteroid and player have different position and should not collide",world.getAsteroids().get(0).getPosition().x,world.getPlayer().getPosition().x,0);
		assertNotEquals("Asteroid and player have different position and should not collide",world.getAsteroids().get(0).getPosition().y,world.getPlayer().getPosition().y,0);
		assertEquals("Size of asteroid arraylist shouldnt change because there was no collision",size,world.getAsteroids().size(),0);
	}
	
	@Test
	public void testPlayerWithAsteroidCollision()  
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(200,200,2,NumGen));
		world.PlayerAsteroidCollision();
		
		assertTrue("Player should be hit because there was collision",world.getPlayer().isHit());
	}
	@Test
	public void testAsteroidWithPlayerCollision()  
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(200,200,2,NumGen));
		int size = world.getAsteroids().size();
		world.PlayerAsteroidCollision();
		
		assertEquals("Asteroid and player have the same position and should collide",world.getAsteroids().get(0).getPosition().x,world.getPlayer().getPosition().x,0);
		assertEquals("Asteroid and player have the same position and should collide",world.getAsteroids().get(0).getPosition().y,world.getPlayer().getPosition().y,0);
		assertNotEquals("Size of asteroid arraylist should change because there was collision",size,world.getAsteroids().size(),0);
	}
		
	@Test
	public void testBulletWithoutAsteroidCollision() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getBullet().add(new Bullet(200,200,0));
		world.getAsteroids().add(new Asteroid(0,0,1,NumGen));
		world.BulletAsteroidCollision();
		assertEquals("Size of bullet arraylist shouldnt change because there was no collision",1,world.getBullet().size(),0);
	}
	@Test
	public void testAsteroidWithoutBulletCollision() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getBullet().add(new Bullet(200,200,0));
		world.getAsteroids().add(new Asteroid(0,0,1,NumGen));
		world.BulletAsteroidCollision();
		assertEquals("There is a MEDIUM asteroid",1,world.getAsteroids().get(0).getType(),0);
		assertNotEquals("Asteroid and bullet have different position and should not collide",world.getAsteroids().get(0).getPosition().x,world.getBullet().get(0).getPosition().x,0);
		assertNotEquals("Asteroid and bullet have different position and should not collide",world.getAsteroids().get(0).getPosition().y,world.getBullet().get(0).getPosition().y,0);
		assertEquals("Size of asteroid arraylist shouldnt change because there was no collision",1,world.getAsteroids().size(),0);
	}
	@Test
	public void testBulletWithAsteroidCollision() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(200,200,2,NumGen));
		world.getBullet().add(new Bullet(200,200,0));
		
		world.BulletAsteroidCollision();
		assertNotEquals("Size of bullet arraylist should change because there was collision",1,world.getBullet().size(),0);
		assertEquals("Score should change because BIG asteroid was destroyed",20,world.getPlayer().getScore(),0);
	}
	
	@Test
	public void testAsteroidWithBulletCollision() 
	{
		world.createWorld();
		world.getAsteroids().clear();
		world.getAsteroids().add(new Asteroid(200,200,2,NumGen));
		world.getBullet().add(new Bullet(200,200,0));
			
		float xA=world.getAsteroids().get(0).getPosition().x;
		float yA=world.getAsteroids().get(0).getPosition().y;
		float xB=world.getAsteroids().get(0).getPosition().y;
		float yB=world.getBullet().get(0).getPosition().y;

		world.BulletAsteroidCollision();
		assertEquals("Asteroid and bullet have the same position and should collide",xA,xB,0);
		assertEquals("Asteroid and bullet have the same position and should collide",yA,yB,0);
		assertNotEquals("Size of asteroid arraylist should change because there was collision",1,world.getAsteroids().size(),0);
	}
}