package com.proz.asteroids.model;

import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.proz.asteroids.AsteroidGame;
import com.proz.asteroids.model.Bullet;
import com.proz.asteroids.model.Player;

public class PlayerTest {

	Player plyr0;
	Player plyr1;
	Player plyr2;
	ArrayList<Bullet> bullets;
	
	@BeforeClass
	public static void initializeWorld()
	{
		AsteroidGame.WIDTH=400;
		AsteroidGame.HEIGHT=400;
	}
	@Before
	public void initializeObjects()
	{
		bullets = new ArrayList<Bullet>();
		plyr0 = new Player(null);
		plyr1 = new Player(bullets);
		plyr2 = new Player(null);
	}
	
	@Test
	public void testLeftRotationEnabled() 
	{
		plyr0.setLeftRotationEnabled(true);

		assertTrue("Player ship should rotate left",plyr0.getLeftRotation());
		assertFalse("Player ship shouldnt rotate right",plyr0.getRightRotation());
	}
	@Test
	public void testRightRotationEnabled() 
	{
		plyr0.setRightRotationEnabled(true);

		assertTrue("Player ship should rotate right",plyr0.getRightRotation());
		assertFalse("Player ship shouldnt rotate left",plyr0.getLeftRotation());
	}
	@Test
	public void testRotationDisabledMovement()
	{
		plyr0.rotate(1.0f);
		assertEquals("Player ship shouldnt move bacause none rotation is enabled",1.57075,plyr0.getRadians(),0.01);
	}
	@Test
	public void testRotationLeftEnabledMovement()
	{
		plyr0.setLeftRotationEnabled(true);
		plyr0.rotate(1.0f);
		assertEquals("Player ship should rotate left radians=4.571",4.571,plyr0.getRadians(),0.01);
	}

	@Test
	public void testRightRotationEnabledMovement() 
	{
		plyr0.setRightRotationEnabled(true);
		plyr0.rotate(1.0f);
		assertEquals("Player ship should rotate right radians=-1.429",-1.429,plyr0.getRadians(),0.01);
	}

	@Test
	public void testBothRotationEnabled() 
	{
		plyr0.setLeftRotationEnabled(true);
		plyr0.setRightRotationEnabled(true);

		assertTrue("Player ship should rotate left",plyr0.getLeftRotation());
		assertTrue("Player ship should rotate right",plyr0.getRightRotation());
	}
	@Test
	public void testBothRotationMovement() 
	{
		plyr0.setLeftRotationEnabled(true);
		plyr0.setRightRotationEnabled(true);
		plyr0.rotate(1.0f);
		assertEquals("Player ship rotates both left and right so radians=1.57075 shouldnt change after rotation",1.57075,plyr0.getRadians(),0.01);
	}
	
	@Test
	public void testSetForwardMovementDisabled() 
	{
		plyr0.setPosition(1.0f);
		assertEquals("Player shouldnt move because ForwardMovement isnt enabled, x=200.0",200.0,plyr0.getPosition().x,0.01);
		assertEquals("Player shouldnt move because ForwardMovement isnt enabled, y=200.0",200.0,plyr0.getPosition().y,0.01);		
	}
	
	@Test
	public void testAccelerationDisabled()
	{
		plyr0.acceleration(1.0f);
		assertEquals("Player shouldnt move because ForwardMovement isnt enabled",200.0,plyr0.getPosition().x,0);
		assertEquals("Player shouldnt move because ForwardMovement isnt enabled",200.0,plyr0.getPosition().y,0);
	}
	
	@Test
	public void testAccelerationEnabledSetPositionDisabled() 
	{
		plyr0.setForwardMovementEnabled(true);
		plyr0.acceleration(1.0f);
		assertEquals("Player shouldnt move because position isnt set after accelaration",200.0,plyr0.getPosition().x,0);
		assertEquals("Player shouldnt move because position isnt set after acceleration",200.0,plyr0.getPosition().y,0);
	}
	@Test
	public void testSetForwardMovementEnabled() 
	{
		plyr0.setForwardMovementEnabled(true);
		plyr0.acceleration(1.0f);
		plyr0.setPosition(1.0f);
		assertEquals("Player should move straight and x shouldnt change more than x=200.0 ",200.0,plyr0.getPosition().x,0.1);
		assertEquals("Player should move straight and y should change to y=400.0 ",400.0,plyr0.getPosition().y,0.01);		
	}
	
	@Test
	public void testShootNoBullets() 
	{
		plyr0.shoot(); //unable to shoot
		assertEquals("Player0 has no bullets array",null,plyr0.getPlayerBullets());
	}
	@Test
	public void testCanShootIsntHit() 
	{
		plyr1.shoot();
		assertFalse("Player1 is not hit so can shoot",plyr1.isHit());
		assertEquals("Player1 should shoot 1 bullet",1,plyr1.getPlayerBullets().size(),0);
	}
	@Test
	public void testCantShootIsHit() 
	{
		plyr1.hitIndicated();
		plyr1.shoot();
		assertTrue("Player1 is hit so cant shoot",plyr1.isHit());
		assertEquals("Player1 should shoot 0 bullet",0,plyr1.getPlayerBullets().size(),0);
	}
	
	@Test
	public void testShootTooMuchBullets() 
	{
		for(int i=0;i<10;i++)
		{
			plyr1.shoot();
		}
		assertEquals("Player1 can shoot only max 4 bullets",4,plyr1.getPlayerBullets().size(),0);
	}

	@Test
	public void testSetLife() 
	{
		plyr0.setLife(-1);
		plyr1.setLife(Integer.MAX_VALUE);
		
		assertEquals("Life cannot be set less than 0",0,plyr0.getLifes(),0);
		assertEquals("Life can be any integer value",Integer.MAX_VALUE,plyr1.getLifes(),0);
	}

	@Test
	public void testAddLife() 
	{
		plyr0.addLife(Integer.MIN_VALUE);
		assertEquals("Player has at start 3 lifes and it cannot drop below zero (-1 means dead)",-1,plyr0.getLifes(),0);
	}

	@Test
	public void testLoseOneLife() 
	{
		plyr0.loseLife();
		plyr1.addLife(-1);
		
		assertEquals("Player addLife(-1) and loseLife() should return the same value ExtraLifes=2",plyr0.getLifes(),plyr1.getLifes(),0);
	}
	
	@Test
	public void testLoseAllExtraLifes() 
	{
		for(int i=0;i<6;i++)
		{
			plyr0.loseLife();
		}
		plyr1.addLife(-10);
		assertEquals("Player lost every ExtraLifes and should have constant value ExtraLifes=-1",plyr0.getLifes(),plyr1.getLifes(),0);
	}

	@Test
	public void testStartingScore() 
	{
		assertEquals("Player current score should be 0",0,plyr0.getScore(),0);
	}
	
	@Test
	public void testExtraLivesNotEnoughtScore() 
	{
		plyr0.increaseScore(900);
		plyr0.checkExtraLives();
		assertEquals("Player current ExtraLives shouldnt change",3,plyr0.getLifes(),0);
	}
	@Test
	public void testCheckExtraLivesEnoughtScore() 
	{
		plyr0.increaseScore(10000);
		plyr0.checkExtraLives();
		assertEquals("Player current ExtraLives should change to 4",4,plyr0.getLifes(),0);
	}

	@Test
	public void testIncreaseScore() 
	{
		plyr0.increaseScore(900);
		plyr1.increaseScore(Integer.MIN_VALUE);
		assertEquals("Player current score should be 900",900,plyr0.getScore(),0);
		assertEquals("Player1 current score shouldnt change beacause points in game are only positive values",0,plyr1.getScore(),0);
	}

	@Test
	public void testIsDead() 
	{
		plyr1.hitIndicated();
		plyr1.update(2.5f);
		assertFalse("Player0 wasnt hit so isnt dead",plyr0.isDead());
		assertTrue("Player1 was hit so is dead",plyr1.isDead());
	}
	@Test
	public void testIsDeadAfterReset() 
	{
		plyr1.hitIndicated();
		plyr1.update(2.5f);
		plyr1.reset();
		assertFalse("Player1 is alive again so isnt dead",plyr1.isDead());
	}
	
	@Test
	public void testIsHit() 
	{
		plyr1.hitIndicated();
		assertFalse("Player0 is in game so hasn been hit yet",plyr0.isHit());
		assertTrue("Player1 was hit",plyr1.isHit());

	}
	@Test
	public void testIsHitAfterReset() 
	{
		plyr1.hitIndicated();
		plyr1.reset();
		assertFalse("Player1 shouldnt be hit after reset",plyr1.isHit());
	}

	@Test
	public void testPlayerStartingPosition() 
	{
		assertEquals("Player default position should be in the middle of the screen x=200",AsteroidGame.WIDTH/2,plyr0.getPosition().x,0);
		assertEquals("Player default position should be in the middle of the screen y=200",AsteroidGame.HEIGHT/2,plyr0.getPosition().y,0);
	}
	@Test
	public void testPlayerStartingStatus() 
	{
		assertEquals("player has 3 lives",3,plyr0.getLifes(),0);
		assertFalse("Player isnt hit",plyr0.isHit());
		assertFalse("Player isnt dead",plyr0.isDead());
	}
	
	@Test
	public void testPositionAfterReset() 
	{
		plyr0.setPosition(100, 100);
		plyr0.reset();
		assertEquals("Player position after reset should be x=WIDTH/2",AsteroidGame.WIDTH/2,plyr0.getPosition().x,0);
		assertEquals("Player position after reset should be y=HIGHT/2",AsteroidGame.HEIGHT/2,plyr0.getPosition().y,0);
	}

	@Test
	public void testScoreUpdateInUpdateLoop() 
	{
		plyr0.increaseScore(10000);
		plyr0.update(1.0f);
		assertEquals("Player should have now 4 lives",4,plyr0.getLifes(),0);
	}
	
	@Test
	public void testPositionUpdateInUpdateLoop() 
	{
		plyr0.setForwardMovementEnabled(true);
		plyr0.setPosition(400, 400);
		plyr0.update(1.0f);

		assertEquals("Player position after update and wrap should be x=0",0,plyr0.getPosition().x,0);
		assertEquals("Player position isafter update and wrap should be y=0",0,plyr0.getPosition().y,0);
	}

	@Test
	public void testNoSpeedDeacceleration() 
	{
		plyr0.setForwardMovementEnabled(true);
		plyr0.deacceleration(1.0f);
		plyr0.setPosition(1.0f);
		assertEquals("Player shouldnt move because you cannot deaccelerate with no speed",200.0,plyr0.getPosition().x,0);
		assertEquals("Player shouldnt move because you cannot deaccelerate with no speed",200.0,plyr0.getPosition().y,0);
	}
	
	@Test
	public void testDeacceleration() 
	{
		plyr0.setForwardMovementEnabled(true);
		plyr0.acceleration(1.0f);
		plyr0.deacceleration(1.0f);
		plyr0.setPosition(1.0f);
		
		System.out.println(plyr0.getPosition().x);
		System.out.println(plyr0.getPosition().y);
		assertEquals("Player should move and x shouldnt change because we are moving only in y axis,x=200.0",200.0,plyr0.getPosition().x,0.1);
		assertEquals("Player should move and y should be equal to y=350.0",350,plyr0.getPosition().y,0.01);
	}
	
	@Test
	public void testSetPositionXY() 
	{
		plyr0.setPosition(10, 10);
		plyr1.setPosition(Float.POSITIVE_INFINITY, 10.0f);
		plyr2.setPosition(10, Float.NEGATIVE_INFINITY);
		
		assertEquals("Player0 has set position x=10 is x=10",10,plyr0.getPosition().x,0);
		assertEquals("Player0 has set position y=10 is y=10",10,plyr0.getPosition().y,0);
		assertEquals("Player1 has set position x=POSITIVE_INFINITY is x=POSITIVE_INFINITY",Float.POSITIVE_INFINITY,plyr1.getPosition().x,0);
		assertEquals("Player1 has set position y=10.0f is y=10.0f",10.0f,plyr1.getPosition().y,0);
		assertEquals("Player2 has set position x=10.0f is x=10.0f",10.0f,plyr2.getPosition().x,0);
		assertEquals("Player2 has set position y=NEGATIVE_INFINITY is y=NEGATIVE_INFINITY",Float.NEGATIVE_INFINITY,plyr2.getPosition().y,0);
	}
	
	@Test
	public void testUpdateAfterSetPositionXY() 
	{
		plyr0.setPosition(10, 10);
		plyr1.setPosition(Float.POSITIVE_INFINITY, 10.0f);
		plyr2.setPosition(10, Float.NEGATIVE_INFINITY);

		plyr0.update(0.0f);
		plyr1.update(0.0f);
		plyr2.update(0.0f);
		
		assertEquals("Player0 has set position x=10 after update is x=10",10,plyr0.getPosition().x,0);
		assertEquals("Player0 has set position y=10 after udpate is y=10",10,plyr0.getPosition().y,0);
		assertEquals("Player1 has set position x=POSITIVE_INFINITY after update is x=0",0,plyr1.getPosition().x,0);
		assertEquals("Player1 has set position y=10.0f after update is y=10.0f",10.0f,plyr1.getPosition().y,0);
		assertEquals("Player2 has set position x=10.0f after update is x=10.0f",10.0f,plyr2.getPosition().x,0);
		assertEquals("Player2 has set position y=NEGATIVE_INFINITY after update is y=HEIGHT",AsteroidGame.HEIGHT,plyr2.getPosition().y,0);		
	}
}